import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { UserRegister } from "./pages/auth/UserRegister";
import { UserLogin } from "./pages/auth/UserLogin";
import { Landing } from './pages/Landing';
import { routes } from './hooks/useRoute';
import { CompanyLogin } from "./pages/auth/CompanyLogin";
import { CompanyRegister } from "./pages/auth/CompanyRegister";
import { IStore, storeModel } from './core/store/store';
import { Popup } from './components/organisms/Popup';
import { UserMainPage } from './pages/userContent/UserMainPage';
import { Chats } from './pages/chat/Chats';
import { ApiContext } from './core/contexts/ApiContext';
import apiClient from './core/api/api-client';
import { DoctorLogin } from './pages/auth/DoctorLogin';
import { StoreProvider, createStore } from 'easy-peasy';
import { UserAppointmentsPage } from './pages/userContent/UserAppointmentsPage';
import { UserInfoPage } from './pages/userContent/UserInfoPage';
import HospitalsDoctors from './pages/hospital/HospitalsDoctors';
import HospitalCabinet from './pages/hospital/HospitalCabinet';
import DoctorAppointments from './pages/doctor/DoctorAppointments';
import DoneDoctorAppointments from './pages/doctor/DoneDoctorAppointments';

function App(){
    const store = createStore<IStore>(storeModel);

    return (
        <StoreProvider store={store}>
          <ApiContext.Provider value={apiClient}>
            <Popup />
            <BrowserRouter>
              <Routes>
                <Route path={routes.userRegisterPage} element={<UserRegister/>}/>
                <Route path={routes.userLoginPage} element={<UserLogin/>}/>
                <Route path={routes.doctorLoginPage} element={<DoctorLogin/>}/>
                <Route path={routes.companyLoginPage} element={<CompanyLogin/>}/>
                <Route path={routes.companyRegisterPage} element={<CompanyRegister/>}/>
                <Route path={routes.landingPage} element={<Landing/>}/>
                <Route path={routes.userMainPage} element={<UserMainPage/>}/>
                <Route path={routes.userAppointments} element={<UserAppointmentsPage/>}/>
                <Route path={routes.userInfo} element={<UserInfoPage/>}/>
                <Route path={routes.chats} element={<Chats/>}/>
                <Route path={routes.hospitalsDoctorPage} element={<HospitalsDoctors/>}/>
                <Route path={routes.hospitalCabinet} element={<HospitalCabinet/>}/>
                <Route path={routes.currentDoctorAppointments} element={<DoctorAppointments/>}/>
                <Route path={routes.doneDoctorAppointments} element={<DoneDoctorAppointments/>}/>
              </Routes>
            </BrowserRouter>
          </ApiContext.Provider>
        </StoreProvider>
    )
}

export default App;
