import { ReactElement, ReactNode, useCallback } from 'react';
import { Box, Button as ButtonComp, ButtonPropsVariantOverrides, SxProps, Theme, Typography } from '@mui/material';
import { useStoreActions } from '../hooks';

export function Button(props: ButtonProps) {
    const open = useStoreActions(actions => actions.popupModel.open);

    const onClick = useCallback(() => {
        open({
            render: () => (
            <>
                {props.renderComp}
            </>
            )
        });
    }, [open]);

    return (
        <ButtonComp onClick={onClick} 
            variant='outlined'
            startIcon={props.startIcon}
            sx={props.sx}
        >
            {props.children}
        </ButtonComp>
    );
};

export type ButtonProps = {
    renderComp: string | number | boolean | ReactElement | Iterable<ReactNode> | null | undefined,
    children?: string | number | boolean | ReactElement | Iterable<ReactNode> | null | undefined,
    startIcon?: ReactNode,
    sx?: SxProps<Theme> | undefined
}