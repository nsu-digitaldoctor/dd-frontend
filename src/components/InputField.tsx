import { SxProps, TextField, TextFieldVariants, Theme } from "@mui/material";
import { useCallback, useMemo, useState } from "react";

type InputFieldProps = {
    value: string,
    label: string,
    onChange: (val: string) => void,
    required?: boolean,
    autoComplete?: string
    autoFocus?: boolean,
    errorHandler?:{
        errorText: string,
        errorChecker: (val:string) => boolean
    },
    sx?: SxProps<Theme> | undefined
}

function useOnChange(props: InputFieldProps){
    const [errorMessage, setErrorMessage] = useState("");

    const onFieldChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        props.onChange(event.target.value);
        if(props.errorHandler) {
            if (!props.errorHandler.errorChecker(event.target.value)) {
                setErrorMessage(props.errorHandler.errorText);
            }else{
                setErrorMessage("");
            }
        }
    }, [props]);

    return useMemo(() => {
        return{
            errorMessage,
            onFieldChange
        }
    }, [errorMessage, onFieldChange]) ;
}

function useInputProps(props: InputFieldProps){
    const {errorMessage, onFieldChange} = useOnChange(props);
    
    return useMemo(() => {
        return{
            variant: 'filled' as TextFieldVariants,
            error: errorMessage !== "",
            required: props.required,
            fullWidth: true,
            value: props.value,
            label: props.label,
            autoComplete: props.autoComplete,
            onChange: onFieldChange,
            autoFocus: props.autoFocus,
            helperText: errorMessage,
            margin: "normal" as "normal" | "none" | "dense" | undefined,
            sx: props.sx
        }
    }, [errorMessage, onFieldChange, props]);
}

function Input(props: InputFieldProps){
    return(
        <TextField
            {...useInputProps(props)}
        />
    );
}

function Password(props: InputFieldProps){
    return(
        <TextField
            {...useInputProps(props)}
            type="password"
        />
    );
}

function Text(props: InputFieldProps){
    return(
        <TextField
            {...useInputProps(props)}
            multiline
            rows={5}
        />
    )
}

export const InputField = {
    Input,
    Password,
    Text
}