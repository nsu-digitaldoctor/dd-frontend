import { createUseStyles } from 'react-jss';

export default createUseStyles({
  text: {
    fontSize: 14,
    lineHeight: '18px',
    letterSpacing: 0.15,
  },

  innerHtmlText: {
    fontSize: 16,
    lineHeight: '1.3',
    wordBreak: 'break-word',

    '& b': {
      fontWeight: 700,
    },
  },
});
