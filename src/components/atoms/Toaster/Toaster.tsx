import { FC } from 'react';

import { useTranslation } from '../../../hooks';
import { IToaster } from '../../../types';

import useStyles from './Toaster.styles';


const Toaster: FC<IToaster> = ({
  title,
  errorCode,
  requestName,
  messageCode,
  messageVariables,
  innerHtmlText,
  children,
  ...rest
}) => {
  const classes = useStyles();

  const { t: tApi } = useTranslation('api');
  const { t: tMessages } = useTranslation('toaster-messages');

  const toasterTitle = requestName
    ? tApi([
        `${requestName}.errors.${errorCode}`,
        `errors.${errorCode}`,
        `${requestName}.errors.defaultMessage`,
        'errors.defaultMessage',
      ])
    : title;

  return (
    <div title={toasterTitle} {...rest}>
      {messageCode && (
        <div className={classes.text}>
          {tMessages(messageCode, messageVariables ?? {})}
        </div>
      )}
      {innerHtmlText && (
        <div
          className={classes.innerHtmlText}
          dangerouslySetInnerHTML={{ __html: innerHtmlText }}
        />
      )}
      {children}
    </div>
  );
};

export default Toaster;
