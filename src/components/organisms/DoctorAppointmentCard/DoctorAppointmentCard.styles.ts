import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: 1050,
    height: 56,
    borderBottom: [1, 'solid', '#1d1b1f0a'],
    borderTop: [1, 'solid', '#1d1b1f0a']
  },
});