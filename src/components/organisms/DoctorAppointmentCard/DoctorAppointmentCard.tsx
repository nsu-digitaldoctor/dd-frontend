import { Button, Typography } from "@mui/material";
import {format} from 'date-fns';
import { ru } from 'date-fns/locale';
import { FC } from "react";
import { useStoreActions } from "../../../hooks";
import { useApi } from "../../../hooks/use-api";
import { EAppointmentsStatus } from "../../../types";
import { Appointment } from "../../../types/appointmentsTypes";
import CreateConclusion from "./components/CreateConclusion";

import useStyles from './DoctorAppointmentCard.styles';

export interface IDoctorAppointmentCardProps {
  appointment: Appointment;
  isCurrentAppointments: boolean;
}

const DoctorAppointmentCard: FC<IDoctorAppointmentCardProps> = ({ appointment, isCurrentAppointments }) => {
  const classes = useStyles();
  const api = useApi();

  const open = useStoreActions(actions => actions.popupModel.open);

  const date = format(appointment.date, 'dd MMMM', {locale: ru});
  const time = format(appointment.date, 'hh:mm');
  const fullUsername = [appointment.user.lastName, appointment.user.firstName, appointment.user.middleName].join(' ');

  const acceptAppointment = async () => {
    await api.updateAppointmentStatus(appointment.id, {status: EAppointmentsStatus.ACCEPTED})
    window.location.reload();
  }

  const rejectAppointment = async () => {
    await api.updateAppointmentStatus(appointment.id, {status: EAppointmentsStatus.REJECTED})
    window.location.reload();
  }

  const AppointmentButton = ({hasConclusion, status}: {hasConclusion: boolean, status: EAppointmentsStatus}) => {
    if (hasConclusion) {
      return (
        <Button
          onClick={() => open({
            render: () => (
              <div>
                <Typography
                  variant='h5'
                  sx={{
                    marginTop: '20px',
                    marginBottom: '35px',
                    height: "3vh"
                  }}
                  fontWeight="bold"
                >
                  Заключение
                </Typography>
                {appointment.conclusion}
              </div>
            ),
            size: 's'
          })}
          sx={{ textTransform: 'none'}}
        >
          Посмотреть
        </Button>
      );
    }

    if (status === EAppointmentsStatus.REJECTED) {
      return null;
    }

    return (
      <Button
        onClick={() => open({
          render: () => (
            <CreateConclusion appointmentId={appointment.id}/>
          ),
        })}
        sx={{ textTransform: 'none'}}
      >
        Написать заключение
      </Button>
    );
  }

  return (
    <div className={classes.root}>
      <span>{date}</span>
      <span>{time}</span>
      <span>{appointment.doctor.hospitalName}</span>
      <span>{fullUsername}</span>
      <span>{appointment.application}</span>
      {!isCurrentAppointments 
        ? <AppointmentButton hasConclusion={Boolean(appointment.conclusion)} status={appointment.status}/>
        : appointment.status === EAppointmentsStatus.OPEN &&
        <>
          <Button
            onClick={acceptAppointment}
            sx={{ textTransform: 'none' }}
          >
            Подтвердить
          </Button><Button
            onClick={rejectAppointment}
            color='error'
            sx={{ textTransform: 'none' }}
          >
            Отклонить
          </Button>
        </>
      }
    </div>
  );
}

export default DoctorAppointmentCard;