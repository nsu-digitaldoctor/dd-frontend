import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    margin: [0, 48, 40, 48],
  },

  head: {
    display: 'flex',
    justifyContent: 'space-between',
    width: 822,
  },

  closeButton: {
    alignSelf: 'flex-start',
  },

  text: {
    color: '#20243A',
    fontWeight: 600,
    lineHeight: '24px',
    letterSpacing: 0.5,
    fontSize: 16,
    marginTop: 10,
    marginBottom: 8,
  }
});