import { Button, IconButton, Typography } from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import { FC, useState } from "react";

import useStyles from './CreateConclusion.styles';
import { InputField } from "../../../InputField";
import { useStoreActions } from "../../../../hooks";
import { useApi } from "../../../../hooks/use-api";
import { EAppointmentsStatus } from "../../../../types";

export interface ICreateConclusionProps {
  appointmentId: number;
}

const CreateConclusion: FC<ICreateConclusionProps> = ({appointmentId}) => {
  const classes = useStyles();
  const api = useApi();
  const close = useStoreActions(actions => actions.popupModel.close);

  const [conclusion, setConclusion] = useState('');

  const addConclusion = async () => {
    await api.addConclusion(appointmentId, {conclusion})
    .then(() => api.updateAppointmentStatus(appointmentId, {status: EAppointmentsStatus.DONE}))
    close();
  }

  return (
    <div className={classes.root}>
      <div className={classes.head}>
        <Typography
          variant='h5'
          sx={{
            marginTop: 4,
            marginBottom: 3,
            height: "4vh"
          }}
          fontWeight="bold"
        >
            Написать заключение
        </Typography>
        <IconButton 
          onClick={() => close()}
          sx={{width: '32px', height: '32px', marginTop: 2}}
        >
          <CloseIcon/>
        </IconButton>
      </div>
      <span className={classes.text}>Содержание заключения</span>
      <InputField.Text
        value={conclusion}
        onChange={setConclusion}
        label='Напишите содержание заключения'
        errorHandler={{
            errorChecker: (val: string) => {return val.length >= 3},
            errorText: "Заключение слишком короткое"
        }}
        sx={{
            paddingRight: '24px'
        }}
      />
      <span className={classes.text}>Документы</span>
      <div className={classes.head}>
        <Button 
          variant="outlined"
          size="large"
          sx={{textTransform: 'none'}}
        >
          Загрузить документы
        </Button>
        <Button 
          variant="contained"
          size="large"
          sx={{textTransform: 'none'}}
          disabled={conclusion.length <= 3}
          onClick={addConclusion}
        >
          Написать
        </Button>
      </div>
    </div>
  );
}

export default CreateConclusion;