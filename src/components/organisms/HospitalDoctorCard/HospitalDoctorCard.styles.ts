import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    width: 965,
    padding: 24,
    '& button:last-child': {
      marginLeft: 'auto',
    }
  },

  doctorsIcon: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: 98,
    height: 98,
    marginRight: 24,
    borderRadius: '50%',
    backgroundColor: '#EEE',
    color: '#49454F',
    fontSize: 30,
    fontWeight: 800,
  },

  doctorInfo: {
    display: 'flex',
    flexDirection: 'column',
    gap: 12,
    '& > div': {
      display: 'flex',
      alignItems: 'center',
    }
  },

  name: {
    fontSize: 22,
    fontWeight: 650,
    color: '#20243A',
    marginRight: 28,
  },

  spec: {
    padding: [4, 12],
    borderRadius: 4,
    backgroundColor: 'rgba(29, 27, 32, 0.04)',
    fontSize: 14,
    fontWeight: 500,
    lineHeight: '24px',
    letterSpacing: 0.5,
  },

  statsContainer: {
    gap: 16
  },

  stats: {
    fontSize: 16,
    fontWeight: 400,
    lineHeight: '24px',
    letterSpacing: 0.5,
    color: '#49454F',
  },

  deleteButton: {
    alignSelf: 'flex-end',
    fontSize: 14,
    fontWeight: 600,
  },

  data: {
    fontWeight: 600,
    color: '#0C5AB2',
  },

  noData: {
    color: '#C13E45',
  }
});