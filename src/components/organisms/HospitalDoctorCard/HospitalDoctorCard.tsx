import { Button } from "@mui/material";
import clsx from "clsx";
import { FC } from "react";
import { ICompanyDoctor } from "../../../core/store/models";
import { useApi } from "../../../hooks/use-api";

import useStyles from './HospitalDoctorCard.styles';

export interface IHospitalDoctorCardProps {
  doctor: ICompanyDoctor;
}

const HospitalDoctorCard: FC<IHospitalDoctorCardProps> = ({ doctor }) => {
  const classes = useStyles();
  const api = useApi();

  const deleteDoctor = async () => {
    await api.deleteHospitalDoctor(doctor.id);
  }

  const doctorInitials = doctor.fullName
  .split(' ')
  .map(word => word.charAt(0))
  .join('')
  .slice(0, -1);

  return (
    <div className={classes.root}>
      <div className={classes.doctorsIcon}>
        {doctorInitials}
      </div>
      <div className={classes.doctorInfo}>
        <div>
          <span className={classes.name}>{doctor.fullName}</span>
          <span className={classes.spec}>{doctor.specialization}</span>
        </div>
        <div className={classes.statsContainer}>
          <div className={classes.stats}>
            <span>Рейтинг:</span>&nbsp;
            <span className={clsx(classes.data, !doctor.rating && classes.noData)}>{doctor.rating ?? 'Нет оценок'}</span>
          </div>
          <div className={classes.stats}>
            <span>Отзывов:</span>&nbsp;
            <span className={clsx(classes.data, !doctor.totalReviews && classes.noData)}>{doctor.totalReviews ?? 'Нет отзывов'}</span>
          </div>
        </div>
        <div>
          <span>{doctor.hospital}</span>
        </div>
      </div>
      <Button
        onClick={deleteDoctor}
        className={classes.deleteButton}
        sx={{color: '#1D1B2061', textTransform: 'none'}}
      >
        Удалить врача
      </Button>
    </div>
  );
}

export default HospitalDoctorCard;