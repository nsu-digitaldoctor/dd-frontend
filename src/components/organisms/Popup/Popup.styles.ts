import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    position: 'fixed',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    zIndex: 1000,
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    lineHeight: 'normal', // Только чтобы очистить knockout-bootstrap
  },

  hidden: {
    visibility: 'hidden',
  },

  modalContainer: {
    marginTop: '10vh',
    padding: [20, 0],
    display: 'flex',
    flexGrow: 1,
    alignItems: 'flex-start',
  },

  modal: {
    boxSizing: 'border-box',
    boxShadow: '0 13px 91px -27px rgba(0, 0, 0, 0.23)',
    border: [1, 'solid', 'lightgray'],
    borderRadius: 16,
    backgroundColor: 'white',
  },

  top: {
    alignSelf: 'flex-start',
  },

  center: {
    alignSelf: 'center',
  },

  bottom: {
    alignSelf: 'flex-end',
  },

  fullScreenContainer: {
    margin: 0,
    padding: 0,
    width: '100%',
  },

  fullScreenModal: {
    boxShadow: 'none',
    border: 'none',
    borderRadius: 0,
    height: '100%',
    width: '100%',
  },

  fullHeightModal: {
    border: 'none',
    borderRadius: 0,
    height: '100%',
    margin: 'auto',
  },

  background: {
    background: 'rgba(34, 42, 55, 0.5)',
  },

  xl: {
    width: 960,
    padding: [36, 40, 40],
  },

  l: {
    width: 560,
    padding: [36, 40, 40],
  },

  m: {
    width: 488,
    padding: [32, 36, 36],
  },

  s: {
    width: 360,
    padding: [20, 24, 24],
  },
});
