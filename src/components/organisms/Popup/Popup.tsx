import clsx from 'clsx';
import { FC, MouseEvent, useEffect } from 'react';


import useStyles from './Popup.styles';
import { useStoreActions, useStoreState } from '../../../hooks';
import { PopupTypes } from '../../../types';

const Popup: FC = () => {
  const classes = useStyles();

  const isOpen = useStoreState(state => state.popupModel.isOpen);
  const popups = useStoreState(state => state.popupModel.popups);

  const close = useStoreActions(actions => actions.popupModel.close);

  // индекс "верхней" модалки для которой надо скрывать предыдущие попапы
  const lastSeparatePopupIndex = popups
    .map(i => i.shouldHidePreviousPopups)
    .lastIndexOf(true);

  const closePopup = () => {
    const lastPopup = popups.length > 0 ? popups[popups.length - 1] : undefined;

    close();
    // Метод close закрывает последнюю модалку, поэтому вызываем её обработчик
    if (lastPopup !== undefined && lastPopup.onClose !== undefined) {
      lastPopup.onClose();
    }
  };

  const onEscClose = (event: KeyboardEvent) => {
    if (event.code === 'Escape') {
      closePopup();
    }
  };

  const stopPropagation = (event: MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();
  };

  useEffect(() => {
    document.addEventListener('keydown', onEscClose);

    return () => {
      document.removeEventListener('keydown', onEscClose);
    };
  });

  // TODO: временный костыль для решения проблемы скролла, выкосить, когда переедем на реакт полностью
  useEffect(() => {
    if (isOpen) {
      document.documentElement.style.overflow = 'hidden';
    } else {
      document.documentElement.style.overflow = '';
    }
  }, [isOpen]);

  return (
    <>
      {popups.map((popup, index) => (
        <div
          key={index}
          className={clsx(classes.root, {
            // скрываем нижние модалки если верхняя должна выводиться отдельно
            [classes.hidden]: index < lastSeparatePopupIndex,
            // по дефолту у всех не FULL_SCREEN модалок должен быть background
            [classes.background]: popup.withBackground ?? popup.type !== PopupTypes.FULL_SCREEN,
          })}
          onClick={closePopup}
        >
          <div
            className={clsx(classes.modalContainer, {
              [classes.fullScreenContainer]:
                popup.type === PopupTypes.FULL_SCREEN ||
                popup.type === PopupTypes.FULL_HEIGHT,
            })}
          >
            <div
              className={clsx(
                classes.modal,
                popup.size && classes[popup.size],
                popup.position && classes[popup.position],
                {
                  [classes.fullScreenModal]:
                    popup.type === PopupTypes.FULL_SCREEN,
                  [classes.fullHeightModal]:
                    popup.type === PopupTypes.FULL_HEIGHT,
                },
              )}
              onClick={stopPropagation}
            >
              {popup.render()}
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default Popup;
