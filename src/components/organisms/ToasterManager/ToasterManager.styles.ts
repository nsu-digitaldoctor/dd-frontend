import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    position: 'fixed',
    top: 0,
    right: 0,
    width: 440,
    pointerEvents: 'none',
    zIndex: 1000,
    padding: 30,
    paddingTop: 20,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },

  toaster: {
    width: 380,
    transition: 'all .2s ease',
    marginBottom: 6,
    pointerEvents: 'auto',
    cursor: 'pointer',
  },

  appearing: {
    transform: 'translateX(100%)',
    transition: 'transform .2s ease',
  },

  disappearing: {
    opacity: 0,
    transition: 'opacity .2s ease',
  },
});
