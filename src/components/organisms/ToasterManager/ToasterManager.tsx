import clsx from 'clsx';
import { FC, useState, useEffect } from 'react';

import { Toaster } from '../../atoms/Toaster';
import { useStoreState, useStoreActions } from '../../../hooks';

import useStyles from './ToasterManager.styles';

const ToasterManager: FC = () => {
  const classes = useStyles();
  const [timestampsList, setTimestampsList] = useState<number[]>([]);

  const toastersList = useStoreState(
    state => state.globalTechModel.toastersList,
  );
  const timestampsListFromStore = useStoreState(
    state => state.globalTechModel.toasterTimestampsList,
  );
  const removeToaster = useStoreActions(
    actions => actions.globalTechModel.removeToaster,
  );

  useEffect(() => {
    setTimestampsList(timestampsListFromStore);
  }, [timestampsListFromStore]);

  return (
    <div className={classes.root}>
      {toastersList.length > 0 &&
        toastersList.map(toaster => (
          <div
            className={clsx(classes.toaster, {
              [classes.appearing]:
                timestampsListFromStore.includes(toaster.timestamp) &&
                !timestampsList.includes(toaster.timestamp),
              
            })}
            key={toaster.timestamp}
            onClick={() => removeToaster(toaster.timestamp)}
          >
            <Toaster
              {...toaster}
            />
          </div>
        ))}
    </div>
  );
};

export default ToasterManager;
