export const BASE_DATE_FORMAT = 'dd.MM.yyyy';
export const BASE_DATE_FORMAT_SHORT_YEAR = 'dd.MM.yy';
export const BASE_TIME_FORMAT = 'HH:mm';
export const DATE_FORMAT_SHORT = 'd MMM';