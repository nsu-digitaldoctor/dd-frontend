import axios, { CancelTokenSource } from 'axios';
import store, { storeModel } from '../store/store';

import config, { THeaders } from './config';
import { API_HOST, API_PORT, AUTH_REQUESTS, HttpStatusCode } from './constants';
import { compilePath, parsePathTemplate } from './helpers';
import mappers, { IAddConclusionRequest, ICreateMessageRequest, IDeleteMessageRequest, IGetDoctorChatsListRequest, IGetMessagesListRequest, IGetUserChatsListRequest, IHospitalLoginRequest, IHospitalRegisterRequest, IRefreshTokenRequest, IRegisterHospitalDoctorRequest, IUpdateAppointment, IUpdateStatusRequest } from './mappers';
import { IUserLoginRequest } from './mappers/auth/login';
import { IUserRegisterRequest } from './mappers/auth/register';
import {
  IApiClientConfig,
  IRequestOptions,
  TRequestParams,
  TRequests,
  IRequestType,
  TRequestNames,
} from './types';
import { ICreateChatRequest } from './mappers/chat/createChat';
import { ICreateAppointmentRequest } from './mappers/user/createAppointment';
import { IUpdateAppointmentRequest } from './mappers/user/updateAppointment';
import { IDeleteAppointmentRequest } from './mappers/user/deleteAppointment';
import { EUserRole } from '../../types';

const storeActions = store.getActions();

export class ApiClient {
  public apiConfig: typeof config;
  private headers?: THeaders;
  private host: string;
  private port?: number | string;
  private requests: TRequests;
  private pendingRequests: Partial<Record<TRequestNames, CancelTokenSource>>;

  constructor(options: IApiClientConfig) {
    this.host = options.host;
    this.port = options.port;
    this.apiConfig = options.apiConfig;

    this.headers = {
      ...options.headers,
      'X-Requested-With': 'XMLHttpRequest',
    };

    this.requests = {};
    this.pendingRequests = {};

    Object.keys(options.apiConfig).forEach((key: string) => {
      this.requests[key] = {
        ...options.apiConfig[key as TRequestNames],
        ...parsePathTemplate(
          options.apiConfig[key as TRequestNames].pathTemplate,
        ),
      };
    });

    storeActions.global.setApiClient(this);
  }

  private static addNewQueryParam(
    url: string,
    paramKey: string,
    paramValue?: string,
  ) {
    let processedUrl = url;

    if (url.indexOf('?') !== -1) {
      processedUrl += `&${paramKey}`;
    } else {
      processedUrl += `?${paramKey}`;
    }

    if (paramValue) {
      processedUrl += `=${paramValue}`;
    }

    return processedUrl;
  }

  private static preventCache(url: string) {
    const hash = Math.random().toString().substring(2);

    return ApiClient.addNewQueryParam(url, hash);
  }

  private static getUrlTail(request?: IRequestType, params?: TRequestParams) {
    if (!request) {
      return '';
    }

    let urlTail = compilePath(request, params);

    if (request.isCacheDisabled) {
      urlTail = ApiClient.preventCache(urlTail);
    }

    return urlTail;
  }

  public setHeaders(headers: THeaders): void {
    this.headers = {
      ...this.headers,
      ...headers,
    };
  }

  // ----------------- REQUEST TYPINGS -------------------
  public register(
    role: EUserRole,
    body: IUserRegisterRequest,
  ): Promise<ReturnType<typeof mappers.emptyMapper>> {
    return this.createRequest({
      name: 'register',
      params: { role },
      body,
      mapper: mappers.emptyMapper,
    });
  }

  public registerHospital(
    body: IHospitalRegisterRequest,
  ): Promise<ReturnType<typeof mappers.emptyMapper>> {
    return this.createRequest({
      name: 'registerHospital',
      body,
      mapper: mappers.emptyMapper,
    });
  }

  public login(
    body: IUserLoginRequest,
  ): Promise<ReturnType<typeof mappers.login>> {
    return this.createRequest({
      name: 'login',
      body,
      mapper: mappers.login,
    });
  }

  public loginHospital(
    body: IHospitalLoginRequest,
  ): Promise<ReturnType<typeof mappers.loginHospital>> {
    return this.createRequest({
      name: 'loginHospital',
      body,
      mapper: mappers.loginHospital,
    });
  }

  public registerHospitalDoctor(
    body: IRegisterHospitalDoctorRequest,
  ): Promise<ReturnType<typeof mappers.emptyMapper>> {
    return this.createRequest({
      name: 'registerDoctor',
      body,
      mapper: mappers.emptyMapper,
    });
  }

  public fetchHospitalDoctors(
    id: number,
  ): Promise<ReturnType<typeof mappers.fetchHospitalDoctors>> {
    return this.createRequest({
      name: 'fetchHospitalDoctors',
      params: { id },
      mapper: mappers.fetchHospitalDoctors,
    });
  }

  public deleteHospitalDoctor(
    id: number,
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'deleteHospitalDoctor',
      params: { id },
      mapper: mappers.emptyMapper
    })
  }

  public refreshToken(
    body: IRefreshTokenRequest,
  ): Promise<ReturnType<typeof mappers.refreshToken>> {
    return this.createRequest({
      name: 'refreshToken',
      body,
      mapper: mappers.refreshToken,
    });
  }

  public account(): Promise<ReturnType<typeof mappers.account>>{
    return this.createRequest({
      name: 'account',
      mapper: mappers.account
    });
  }

  public createChat(
    body: ICreateChatRequest
  ): Promise<ReturnType<typeof mappers.createChat>> {
    return this.createRequest({
      name: 'createChat',
      body,
      mapper: mappers.createChat
    })
  }

  public getUserChatsList(
    body: IGetUserChatsListRequest
  ): Promise<ReturnType<typeof mappers.getUserChatsList>>{
    return this.createRequest({
      name: 'getUserChatsList',
      body,
      mapper: mappers.getUserChatsList
    })
  }

  public getDoctorChatsList(
    body: IGetDoctorChatsListRequest
  ): Promise<ReturnType<typeof mappers.getDoctorChatsList>>{
    return this.createRequest({
      name: 'getDoctorChatsList',
      body,
      mapper: mappers.getUserChatsList
    })
  }

  public updateChat(
    body: IUpdateStatusRequest
  ): Promise<ReturnType<typeof mappers.updateChat>>{
    return this.createRequest({
      name: 'updateChat',
      body,
      mapper: mappers.updateChat
    })
  }

  public getMessagesList(
    body: IGetMessagesListRequest
  ): Promise<ReturnType<typeof mappers.getMessageList>>{
    return this.createRequest({
      name: 'getMessageList',
      body,
      mapper: mappers.getMessageList
    })
  }

  public createMessage(
    body: ICreateMessageRequest
  ): Promise<ReturnType<typeof mappers.createMessage>>{
    return this.createRequest({
      name: 'createMessage',
      body,
      mapper: mappers.createMessage
    })
  }

  public deleteMessage(
    body: IDeleteMessageRequest
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'deleteMessage',
      body,
      mapper: mappers.emptyMapper
    })
  }

  public getUserAppointments(
    id: number
  ): Promise<ReturnType<typeof mappers.getAppointments>>{
    return this.createRequest({
      name: 'getUserAppointments',
      params: {id},
      mapper: mappers.getAppointments
    })
  }

  public getDoctors(): Promise<ReturnType<typeof mappers.getDoctors>>{
    return this.createRequest({
      name: "getDoctors",
      mapper: mappers.getDoctors
    })
  }

  public createAppointment(
    body: ICreateAppointmentRequest
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'createAppointment',
      body,
      mapper: mappers.emptyMapper
    })
  }

  public updateAppointment(
    body: IUpdateAppointmentRequest
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'updateAppointment',
      body,
      mapper: mappers.emptyMapper
    })
  }

  public deleteAppointment(
    body: IDeleteAppointmentRequest
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'deleteAppointment',
      body,
      mapper: mappers.emptyMapper
    });
  }
  
  public fetchHospital(
    id: number,
  ): Promise<ReturnType<typeof mappers.fetchHospital>> {
    return this.createRequest({
      name: 'fetchHospital',
      params: { id },
      mapper: mappers.fetchHospital,
    });
  }

  public fetchDoctorAppointments(
    id: number,
  ): Promise<ReturnType<typeof mappers.fetchDoctorAppointments>> {
    return this.createRequest({
      name: 'fetchDoctorAppointments',
      params: { id },
      mapper: mappers.fetchDoctorAppointments,
    });
  }

  public addConclusion(
    appointmentId: number,
    body: IAddConclusionRequest
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'addConclusion',
      params: {appointmentId},
      body,
      mapper: mappers.emptyMapper
    });
  }

  public updateAppointmentStatus(
    appointmentId: number,
    body: IUpdateAppointment
  ): Promise<ReturnType<typeof mappers.emptyMapper>>{
    return this.createRequest({
      name: 'updateAppointmentStatus',
      params: {appointmentId},
      body,
      mapper: mappers.emptyMapper
    });
  }

  // ------------------------

  public compileUrl({
    name,
    params,
    host,
    port,
  }: {
    name: TRequestNames;
    params?: TRequestParams;
    host?: string;
    port?: number;
  }): string {
    const request = this.requests[name];

    if (!request) {
      throw new Error('No request with this name in config');
    }

    const accessToken = store.getState().userModel.accessToken;
    if (!AUTH_REQUESTS.includes(name)) {
      this.setHeaders({Authorization: 'Bearer ' + accessToken})
      
    }

    const requestHost = host || request.host || this.host;
    const requestPort = port || request.port || this.port;

    return `${requestHost}${
      requestPort !== undefined ? `:${requestPort}` : ''
    }${ApiClient.getUrlTail(request, params)}`;
  }

  public cancelRequest(requestName: TRequestNames, message?: string): void {
    const previousCancelToken = this.pendingRequests[requestName];

    if (previousCancelToken !== undefined) {
      previousCancelToken.cancel(message);
      this.clearToken(requestName);
    }
  }

  public checkIfRequestIsPending(requestName: TRequestNames): boolean {
    return this.pendingRequests[requestName] !== undefined;
  }

  private async createRequest<T>({
    name,
    host,
    port,
    headers,
    body,
    params,
    preloaderMessage,
    shouldShowGlobalPreloader,
    mapper,
  }: IRequestOptions<T>): Promise<T> {
    const request = this.requests[name];

    if (!request) {
      throw new Error('No request with this name in config');
    }
    
    if(!storeModel.userModel.accessToken){
      storeActions.userModel.setAccessToken(localStorage.getItem("accessToken") ?? "");
      storeActions.userModel.setRefreshToken(localStorage.getItem("refreshToken") ?? "");
    }
    const url = this.compileUrl({ name, params, host, port });

    const requestHeaders = {
      ...this.headers,
      ...request.headers,
      ...headers,
    };
    const shouldShowPreloader =
      request.shouldShowGlobalPreloader || shouldShowGlobalPreloader;


    const newCancelToken = axios.CancelToken.source();
    this.pendingRequests[name] = newCancelToken;

    if (shouldShowPreloader) {
      storeActions.globalTechModel.showGlobalPreloader({
        message: preloaderMessage,
        messageCode: request.globalPreloaderMessageCode,
        requestName: name,
      });
    }

    return axios
      .request({
        url,
        data: body,
        headers: requestHeaders,
        method: request.method,
        cancelToken: newCancelToken.token,
      })
      .then(response => {
        if (shouldShowPreloader) {
          storeActions.globalTechModel.hideGlobalPreloader();
        }

        this.clearToken.call(this, name);
        return response;
      })
      .catch(err => {
        if (shouldShowPreloader) {
          storeActions.globalTechModel.hideGlobalPreloader();
        }

        if (axios.isCancel(err)) {
          return { isCanceled: true, message: err.message };
        }

        return Promise.reject(err);
      })
      .then(mapper)
      .catch(err => {
        const originalRequest = err.config;
        if (err.response?.status === HttpStatusCode.Forbidden && !originalRequest._retry) {
          originalRequest._retry = true;
          this.processUpdateTokens();
          const accessToken = storeModel.userModel.accessToken;
          originalRequest.headers.Authorization = `Bearer ${accessToken}`;
          return axios(originalRequest);
        }
        return err;
      })
      .catch(err => {
        if (err.response?.status === HttpStatusCode.NotFound) {
          storeActions.globalTechModel.addToaster({
            type: 'error',
            messageCode: 'request.notFound',
          });
        }

        return Promise.reject(err);
      });
  }

  private clearToken(requestName: TRequestNames) {
    this.pendingRequests[requestName] = undefined;
  }

  private processUpdateTokens() {
    try {
      storeActions.userModel.onUpdateTokens();
    } catch (error) {
      console.error(`api-client/processFeatureFlags: ${error}`);
    }

    return;
  }
}

// eslint-disable-next-line import/no-anonymous-default-export
export default new ApiClient({
  apiConfig: config,
  host: API_HOST,
  port: API_PORT,
});
