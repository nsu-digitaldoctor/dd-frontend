export type THeaders = Record<string, string>;
export type RequestMethodTypes = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE';

export interface IRequestConfigItem {
  pathTemplate: string;
  method: RequestMethodTypes;
  headers?: THeaders;
  host?: string;
  port?: number;
  isCacheDisabled?: boolean;
  /**
   * Нужно ли показывать лоадер
   * @default false
   */
  shouldShowGlobalPreloader?: boolean;
  /**
   * Код сообщения из локали api для лоадера
   */
  globalPreloaderMessageCode?: string;
  /**
   * Нужно ли показывать тостер при ошибке запроса
   * @default false
   */
  shouldShowToasterOnError?: boolean;
  /**
   * Могут ли одновременно существовать несколько одинаковых запросов
   * @default false
   */
  areParallelRequestsPossible?: boolean;
  /**
   * Нужно ли выключить преобразование null в undefined
   * @default false
   */
  shouldPreventNullToUndefMapping?: boolean;
}

const apiConfig = checkConfig({
  register: {
    pathTemplate: '/api/v1/auth/register',
    method: 'POST',
  },

  login: {
    pathTemplate: '/api/v1/auth/login',
    method: 'POST',
  },

  registerHospital: {
    pathTemplate: '/api/v1/hospital/register',
    method: 'POST',
  },

  loginHospital: {
    pathTemplate: '/api/v1/hospital/login',
    method: 'POST',
  },

  fetchHospital: {
    pathTemplate: '/api/v1/hospital/:id',
    method: "GET",
  },

  registerDoctor: {
    pathTemplate: '/api/v1/hospital/registerDoctor',
    method: "POST",
  },

  fetchHospitalDoctors: {
    pathTemplate: '/api/v1/hospital/:id/doctors',
    method: "GET",
  },

  deleteHospitalDoctor: {
    pathTemplate: '/api/doctors/delete/:id',
    method: 'DELETE',
  },
  
  refreshToken: {
    pathTemplate: '/api/v1/auth/token/refresh',
    method: 'POST',
  },

  account: {
    pathTemplate: '/api/v1/account',
    method: 'GET'
  },

  createChat: {
    pathTemplate: '/chat/v1/createChat',
    method: 'POST'
  },

  getUserChatsList: {
    pathTemplate: '/chat/v1/getUserChatsList',
    method: 'POST'
  },

  getDoctorChatsList: {
    pathTemplate: '/chat/v1/getChatsList',
    method: 'POST'
  },

  updateChat: {
    pathTemplate: '/chat/v1/updateChat',
    method: 'POST'
  },

  getMessageList: {
    pathTemplate: '/chat/v1/getMessagesList',
    method: 'POST'
  },

  createMessage: {
    pathTemplate: '/chat/v1/createMessage',
    method: 'POST'
  },

  deleteMessage: {
    pathTemplate: '/chat/v1/deleteMessage',
    method: 'DELETE'
  },

  getUserAppointments: {
    pathTemplate: '/appointments/by-user/:id',
    method: 'GET'
  },

  getDoctors: {
    pathTemplate: '/api/doctors',
    method: 'GET'
  },

  createAppointment: {
    pathTemplate: '/appointments/create',
    method: 'POST'
  },

  updateAppointment: {
    pathTemplate: '/appointments/update',
    method: 'PUT'
  },

  deleteAppointment: {
    pathTemplate: '/appointments/delete',
    method: 'DELETE'
  },

  fetchDoctorAppointments: {
    pathTemplate: '/appointments/by-doctor/:id',
    method: 'GET',
  },

  addConclusion: {
    pathTemplate: '/appointments/insertConclusion/:appointmentId',
    method: 'POST',
  },

  updateAppointmentStatus: {
    pathTemplate: '/appointments/updateStatus/:appointmentId',
    method: 'POST',
  }
});

function checkConfig<P extends string>(config: Record<P, IRequestConfigItem>) {
  return config;
}

export default apiConfig;