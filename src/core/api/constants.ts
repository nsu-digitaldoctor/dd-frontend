import { ToasterType } from "../../types";
import { TFaultLevel } from "./types";

export const HttpStatusCode = {
  OK: 200,
  Forbidden: 403,
  NotFound: 404,
  Conflict: 409,
  BadGateway: 502,
} as const;

export const ToasterTypesByFaultLevel: Record<TFaultLevel, ToasterType> = {
  0: 'not-ok',
  1: 'info',
  2: 'warning',
  3: 'error',
};

export const AUTH_REQUESTS = ['register', 'login', 'refreshToken', 'registerHospital', 'loginHospital'];

export const AUTH_HEADER = 'Authorization';

export const API_HOST = 'http://93.92.223.199';
export const API_PORT = 8381;