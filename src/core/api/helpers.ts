import { IPathConfig, TRequestParams } from "./types";

export const compilePath = (
  pathConfig: IPathConfig,
  requestOptions?: TRequestParams,
): string => {
  let { path } = pathConfig;
  const remainingRequestOptions = requestOptions ? { ...requestOptions } : {};

  if (pathConfig.keys) {
    pathConfig.keys.forEach(key => {
      if (!requestOptions || !requestOptions[key]) {
        throw new Error('No necessary key provided');
      }

      path = path.replace(`:${key}`, String(requestOptions[key]));
      delete remainingRequestOptions[key];
    });
  }

  if (pathConfig.unnecessaryKeys) {
    pathConfig.unnecessaryKeys.forEach(unnecessaryKey => {
      if (!requestOptions || !requestOptions[unnecessaryKey]) {
        path = path.replace(`/:${unnecessaryKey}?`, '');
      } else {
        path = path.replace(
          `:${unnecessaryKey}?`,
          String(requestOptions[unnecessaryKey]),
        );
        delete remainingRequestOptions[unnecessaryKey];
      }
    });
  }

  if (Object.keys(remainingRequestOptions).length > 0) {
    path = `${path}?`;

    Object.keys(remainingRequestOptions).forEach(queryParam => {
      if (
        remainingRequestOptions[queryParam] === null ||
        remainingRequestOptions[queryParam] === undefined
      ) {
        return;
      }

      const encodeParam = encodeURIComponent(
        String(remainingRequestOptions[queryParam]),
      );

      path = `${path}${queryParam}=${encodeParam}&`;
    });

    path = path.substring(0, path.length - 1);
  }

  return path;
}

export const parsePathTemplate = (pathTemplate: string): IPathConfig => {
  const result: IPathConfig = {
    path: pathTemplate.charAt(0) === '/' ? pathTemplate : `/${pathTemplate}`,
  };

  if (pathTemplate.indexOf(':') !== -1) {
    const allKeys = pathTemplate
      .split('/')
      .filter(part => part.indexOf(':') !== -1)
      .map(key => key.replace(':', ''));

    const keys = [];
    const unnecessaryKeys = [];

    for (const key of allKeys) {
      if (key.indexOf('?') === -1) {
        keys.push(key);
      } else {
        unnecessaryKeys.push(key.replace('?', ''));
      }
    }

    if (keys.length > 0) {
      result.keys = keys;
    }

    if (unnecessaryKeys.length > 0) {
      result.unnecessaryKeys = unnecessaryKeys;
    }
  }

  return result;
}
