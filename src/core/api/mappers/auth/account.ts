export interface IAccountResponseRaw {
    data: {
        id: number,
        firstName: string,
        lastName: string,
        middleName?: string,
        email: string,
        role: string
    }
}

export const account = (response: IAccountResponseRaw): IAccountResponseRaw['data'] => {
    return response.data;
  }