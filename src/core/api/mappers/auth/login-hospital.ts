export interface IHospitalLoginRequest {
  inn: string;
  password: string;
}

export interface IHospitalLoginResponseRaw {
  data: {
    id: number,
    accessToken: string,
    refreshToken: string
  }
}

export const loginHospital = (response: IHospitalLoginResponseRaw): IHospitalLoginResponseRaw['data'] => {
  return response.data;
}
