export interface IUserLoginRequest {
  email: string;
  password: string;
}

export interface IUserLoginResponseRaw {
  data: {
    userId: number,
    accessToken: string,
    refreshToken: string
  }
}

export const login = (response: IUserLoginResponseRaw): IUserLoginResponseRaw['data'] => {
  return response.data;
}
