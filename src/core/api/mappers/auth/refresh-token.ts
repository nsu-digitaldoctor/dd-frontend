export interface IRefreshTokenRequest {
  refreshToken: string;
}

export interface IRefreshTokenResponseRaw {
  data: {
    userId: number;
    accessToken: string;
    refreshToken: string;
  }
}

export const refreshToken = (response: IRefreshTokenResponseRaw): IRefreshTokenResponseRaw['data'] => {
  return response.data;
}
