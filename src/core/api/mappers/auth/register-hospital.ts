export interface IHospitalRegisterRequest {
  name: string;
  inn: string; // ИНН 1234567890
  email: string;
  password: string;
}