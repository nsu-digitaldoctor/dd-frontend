export interface IUserRegisterRequest {
  firstName: string;
  lastName: string
  middleName: string;
  email: string;
  password: string;
}