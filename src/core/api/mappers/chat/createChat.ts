export interface ICreateChatRequest{
    chat: {
        title: string,
        isAnonymous: boolean
    },
    user: {
        id: number
    }
}

export interface ICreateChatResponseRaw{
    data: {
        id: number,
        title: string,
        isAnonymous: boolean,
        status: string,
        createDate: string,
        user: {
            id: number
        }
    }
}

export const createChat = (response: ICreateChatResponseRaw): ICreateChatResponseRaw['data'] => {
    return response.data;
}