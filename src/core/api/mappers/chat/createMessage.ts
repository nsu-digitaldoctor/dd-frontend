export interface ICreateMessageRequest{
    message:{
        chatId: number,
        content: string,
        replyMessageId: number | null
    },
    user: {
        id: number
    }
}

export interface ICreateMessageResponseRaw{
    data: {
        messageId: number,
        chatId: number,
        user: {
            id: number,
            firstName: string,
            lastName: string,
            role: string
        } | null,
        sentDate: string,
        content: string,
        replyMessageId: number | null
    }
}

export const createMessage = (response: ICreateMessageResponseRaw): ICreateMessageResponseRaw['data'] => {
    return response.data;
}