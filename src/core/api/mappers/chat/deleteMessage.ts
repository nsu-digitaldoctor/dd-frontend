export interface IDeleteMessageRequest{
    message: {
        messageId: number
    },
    user:{
        id: number
    }
}