import { EChatStatus } from "../../../../types/chatTypes"

export interface IGetDoctorChatsListRequest{
    page: {
        id: number,
        size: number
    },
    filter: {
        status?: string
    },
    user: {
        id: number
    }
}

export interface IGetDoctorChatsListResponseRaw{
    data: {
        chats: {
            id: number,
            title: string,
            isAnonymous: boolean,
            status: EChatStatus,
            createDate: string,
            user: {
                id: number
            }
        }[]
    }
}

export const getDoctorChatsList = (response: IGetDoctorChatsListResponseRaw): IGetDoctorChatsListResponseRaw['data'] => {
    return response.data;
}