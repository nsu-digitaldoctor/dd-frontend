export interface IGetMessagesListRequest{
    chat: {
        id: number
    },
    user:{
        id: number
    }
}

export interface IGetMessagesListResponseRaw{
    data:{
        messages:{
            messageId: number,
            chatId: number,
            user: {
                id: number,
                firstName: string,
                lastName: string,
                role: string
            } | null,
            sentDate: string,
            content: string,
            replyMessageId: number | null
        }[]
    }
}

export interface IGetMessagesListResponse{
    messages:{
        messageId: number,
        chatId: number,
        user: {
            id: number,
            firstName: string,
            lastName: string,
            role: string
        } | null,
        sentDate: string,
        content: string,
        replyMessageId: number | null
    }[]
}

export const getMessagesList = (response: IGetMessagesListResponseRaw): IGetMessagesListResponseRaw['data'] => {
    return response.data;
}