import { EChatStatus } from "../../../../types/chatTypes"

export interface IGetUserChatsListRequest{
    page: {
        id: number,
        size: number
    },
    filter: {
        status?: string,
        isAnonymous?: boolean
    },
    user: {
        id: number
    }
}

export interface IGetUserChatsListResponseRaw{
    data: {
        chats: {
            id: number,
            title: string,
            isAnonymous: boolean,
            status: EChatStatus,
            createDate: string,
            user: {
                id: number
            }
        }[]
    }
}

export interface IChatInfoResponse{
    chats: {
        id: number,
        title: string,
        isAnonymous: boolean,
        status: EChatStatus,
        createDate: string,
        user: {
            id: number
        }
    }[]
}

export const getUserChatsList = (response: IGetUserChatsListResponseRaw): IGetUserChatsListResponseRaw['data'] => {
    return response.data;
}