export interface IUpdateStatusRequest{
    chats: {
        chatId: number,
        newStatus?: string,
        userId?: number,
        newIsAnonymous?: boolean
    }[],
    user: {
        id: number
    }
}

export interface IUpdateStatusResponseRaw{
    data: {
        chats: {
            id: number,
            title: string,
            isAnonymous: boolean,
            status: string,
            createDate: string,
            user: {
                id: number
            }
        }[]
    }
}

export const updateChat = (response: IUpdateStatusResponseRaw): IUpdateStatusResponseRaw['data'] => {
    return response.data;
}