import { Appointment } from "../../../../types/appointmentsTypes"


export interface IFetchDoctorAppointmentsResponseRaw {
  data: Appointment[];
}

export const fetchDoctorAppointments = (response: IFetchDoctorAppointmentsResponseRaw): 
  IFetchDoctorAppointmentsResponseRaw['data'] => response.data