import { EAppointmentsStatus } from "../../../../types";

export interface IUpdateAppointment {
  status: EAppointmentsStatus;
}