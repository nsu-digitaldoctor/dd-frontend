import { ESpecialization } from "../../../../types";
import { ICompanyDoctor } from "../../../store/models";

export interface IDoctorRaw {
  id: number;
  firstname: string;
  lastname: string;
  middleName: string;
  price: number;
  specialty: ESpecialization;
  hospital: string;
}

export interface IFetchHospitalDoctorsResponseRaw {
  data: {
    doctors: IDoctorRaw[];
  }
}

const getValueByKeyForStringEnum = (value: string) => {
  return Object.entries(ESpecialization).find(([key, val]) => key === value)?.[1];
}

export const fetchHospitalDoctors = (response: IFetchHospitalDoctorsResponseRaw): ICompanyDoctor[] => (
  response.data.doctors.map(doctor => ({
    id: doctor.id,
    fullName: [doctor.lastname, doctor.firstname, doctor.middleName].join(' '),
    price: doctor.price,
    specialization: getValueByKeyForStringEnum(doctor.specialty),
    hospital: doctor.hospital,
  }))
)