export interface IFetchHospitalResponseRaw {
  data: {
    id: number;
    name: string,
    address: string,
    rating: number,
    description: string,
    fullName: string,
  }
}

export const fetchHospital = (response: IFetchHospitalResponseRaw): IFetchHospitalResponseRaw['data'] =>
  response.data;