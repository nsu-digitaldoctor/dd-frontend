export interface IRegisterHospitalDoctorRequest {
  firstName: string;
  lastName: string;
  middleName: string;
  email: string;
  password: string;
  price: number;
  specialty: string;
}
