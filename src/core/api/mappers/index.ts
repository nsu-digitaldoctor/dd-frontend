import { account, login, loginHospital, refreshToken } from "./auth";
import { createChat, createMessage, getDoctorChatsList, getMessagesList, getUserChatsList, updateChat } from "./chat";
import { emptyMapper } from "./empty-mapper";
import { getUserAppointments, getDoctors } from "./user";
import { fetchHospital, fetchHospitalDoctors } from "./hospital";
import { fetchDoctorAppointments } from "./doctor";

const clearParamType = <T>(func: (param: any) => T) => func;

const mappers = {
  emptyMapper: clearParamType(emptyMapper),
  login: clearParamType(login),
  refreshToken: clearParamType(refreshToken),
  account: clearParamType(account),
  createChat: clearParamType(createChat),
  getUserChatsList: clearParamType(getUserChatsList),
  updateChat: clearParamType(updateChat),
  getMessageList: clearParamType(getMessagesList),
  createMessage: clearParamType(createMessage),
  getDoctorChatsList: clearParamType(getDoctorChatsList),
  getAppointments: clearParamType(getUserAppointments),
  getDoctors: clearParamType(getDoctors),
  createAppointment: clearParamType(emptyMapper),
  updateAppointment: clearParamType(emptyMapper),
  deleteAppointment: clearParamType(emptyMapper),
  loginHospital: clearParamType(loginHospital),
  fetchHospital: clearParamType(fetchHospital),
  fetchHospitalDoctors: clearParamType(fetchHospitalDoctors),
  fetchDoctorAppointments: clearParamType(fetchDoctorAppointments),
}

export default mappers;

export * from './auth';
export * from './chat';
export * from './user';
export * from './hospital';
export * from './doctor';
