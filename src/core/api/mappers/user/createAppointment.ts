export interface ICreateAppointmentRequest{
    userId: number,
    doctorId: number,
    application: string,
    date: string
}