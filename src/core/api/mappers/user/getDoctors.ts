import { DoctorInfo } from "../../../../types/appointmentsTypes";

export interface IGetDoctorsResponseRaw{
    data: DoctorInfo[]
}

export const getDoctors = (response: IGetDoctorsResponseRaw) : IGetDoctorsResponseRaw['data'] => {
    return response.data;
}