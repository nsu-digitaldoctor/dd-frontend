import { EAppointmentsStatus } from "../../../../types"
import { DoctorInfo } from "../../../../types/appointmentsTypes"

export interface IGetUserAppointmentsResponseRaw{
    data: {
            id: number,
            user: {
                id: number,
                firstName: string,
                lastName: string,
                middleName?: string
            },
            doctor: DoctorInfo,
            application: string,
            status: EAppointmentsStatus,
            conclusion: string,
            date: string
        }[]
}

export const getUserAppointments = (response: IGetUserAppointmentsResponseRaw): IGetUserAppointmentsResponseRaw["data"] => {
    return response.data;
}