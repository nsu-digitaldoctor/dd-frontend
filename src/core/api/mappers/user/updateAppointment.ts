export interface IUpdateAppointmentRequest{
    id: number,
    userId: number,
    doctorId: number,
    application: string,
    date: string
}