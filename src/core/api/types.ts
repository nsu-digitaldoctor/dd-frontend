import config, { THeaders, IRequestConfigItem } from './config';

// Api client

/**
 * В зависимости от уровня ошибки выводится тостер соответствующего типа.
 * Соответствие типов тостеров и уровней ошибок находится в файле constants
 * для api-client.
 */
export type TFaultLevel = 0 | 1 | 2 | 3;

export type TRequestParams = Record<
  string,
  string | string[] | null | undefined | boolean | number
>;

export interface IPathConfig {
  path: string;
  keys?: string[];
  unnecessaryKeys?: string[];
}

export interface IRequestType extends IPathConfig, IRequestConfigItem {}

export type TRequestNames = keyof typeof config;

export interface IApiClientConfig {
  apiConfig: typeof config;
  host: string;
  port?: number;
  headers?: THeaders;
}

export type TRequests = Record<string, IRequestType>;

export interface IRequestOptions<T> {
  name: TRequestNames;
  host?: string;
  port?: number;
  headers?: THeaders;
  params?: TRequestParams;
  preloaderMessage?: string;
  shouldShowGlobalPreloader?: boolean;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body?: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  mapper: (response: any) => T;
}

export interface ICanceledResponse {
  isCanceled: boolean;
  message?: string;
}
