import { createContext } from 'react';

import type { ApiClient } from '../api/api-client';

export const ApiContext = createContext(undefined as unknown as ApiClient);
