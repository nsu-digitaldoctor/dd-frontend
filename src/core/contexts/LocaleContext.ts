import { createContext } from 'react';

import { TLocale, LOCALES } from '../../i18nConfig';

export const LocaleContext = createContext<TLocale>(LOCALES[0]);
