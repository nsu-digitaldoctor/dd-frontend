import { action, Action } from 'easy-peasy';
import { isBefore } from 'date-fns';
import { EAppointmentsStatus } from '../../../types';
import { Appointment } from '../../../types/appointmentsTypes';

export interface IAppointmentModel {
  allAppointments: Appointment[];
  setAppointments: Action<IAppointmentModel, Appointment[]>;
  currentAppointments: Appointment[];
  needConclusionAppointments: Appointment[];
  archiveAppointments: Appointment[];
}

const CURRENT_STATUSES = [EAppointmentsStatus.OPEN, EAppointmentsStatus.ACCEPTED]
const ARCHIVE_STATUSES = [EAppointmentsStatus.REJECTED, EAppointmentsStatus.DONE]

const appointmentModel: IAppointmentModel = {
  allAppointments: [],
  currentAppointments: [],
  needConclusionAppointments: [],
  archiveAppointments: [],
  setAppointments: action((state, payload) => {
    console.log(payload);
    const apps = payload.map(ap => ({
      ...ap,
      status: CURRENT_STATUSES.includes(ap.status) && isBefore(ap.date, new Date()) ? EAppointmentsStatus.EXPIRED : ap.status,
    }))
    state.allAppointments = apps;
    state.currentAppointments = apps.filter(appointment => CURRENT_STATUSES.includes(appointment.status));
    state.needConclusionAppointments = apps.filter(appointment => appointment.status === EAppointmentsStatus.EXPIRED && !appointment.conclusion);
    state.archiveAppointments = apps.filter(appointment => (appointment.status === EAppointmentsStatus.EXPIRED && appointment.conclusion) || ARCHIVE_STATUSES.includes(appointment.status));
  })
};

export { appointmentModel };
