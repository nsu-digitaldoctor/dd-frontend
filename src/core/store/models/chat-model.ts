import { action, Action } from 'easy-peasy';
import { EAppointmentsStatus } from '../../../types';

export interface IMessage {
  id: number;
  userId: number;
  threadId: number;
  replyMessageId: number;
  message: string;
  sentDate: Date;
  isDeleted: boolean;
}

export interface IChatModel {
  id: number;
  setId: Action<IChatModel, number>;
  userId: number;
  setUserId: Action<IChatModel, number>;
  title: string;
  setTitle: Action<IChatModel, string>;
  isAnonymous: boolean;
  setAnonymous: Action<IChatModel, boolean>;
  status: EAppointmentsStatus;
  setStatus: Action<IChatModel, EAppointmentsStatus>;
  messages: IMessage[];
  setMessages: Action<IChatModel, IMessage[]>;
}

const chatModel: IChatModel = {
  id: 0,
  setId: action((state, payload) => {
    state.id = payload;
  }),
  userId: 0,
  setUserId: action((state, payload) => {
    state.userId = payload;
  }),
  title: '',
  setTitle: action((state, payload) => {
    state.title = payload;
  }),
  isAnonymous: true,
  setAnonymous: action((state, payload) => {
    state.isAnonymous = payload;
  }),
  status: EAppointmentsStatus.OPEN,
  setStatus: action((state, payload) => {
    state.status = payload;
  }),
  messages: [],
  setMessages: action((state, payload) => {
    state.messages = payload;
  }),
};

export { chatModel };
