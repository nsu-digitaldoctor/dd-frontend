import { action, Action } from "easy-peasy";
import { FEATURE_FLAG_NAMES, TFeatureFlags, TLocale, TTheme } from "../../../types";
import type { ApiClient } from "../../api/api-client";

export interface IGlobalModel {
  appLocale: TLocale;
  setAppLocale: Action<IGlobalModel, TLocale>;
  appTheme: TTheme;
  setAppTheme: Action<IGlobalModel, TTheme>;
  featureFlags: TFeatureFlags;
  setFeatureFlags: Action<IGlobalModel, TFeatureFlags>;
  userLocale?: string;
  setUserLocale: Action<IGlobalModel, string>;
  apiClient?: ApiClient;
  setApiClient: Action<IGlobalModel, ApiClient>;
}

const globalModel: IGlobalModel = {
  appLocale: 'ru',
  setAppLocale: action((state, payload) => {
    state.appLocale = payload;
  }),
  appTheme: 'light',
  setAppTheme: action((state, payload) => {
    state.appTheme = payload;
  }),
  featureFlags: Object.fromEntries(
    FEATURE_FLAG_NAMES.map(name => [name, false]),
  ) as TFeatureFlags,
  setFeatureFlags: action((state, payload) => {
    state.featureFlags = { ...payload };
  }),
  setUserLocale: action((state, payload) => {
    state.userLocale = payload;
  }),
  setApiClient: action((state, payload) => {
    state.apiClient = payload;
  }),
}

export { globalModel };
