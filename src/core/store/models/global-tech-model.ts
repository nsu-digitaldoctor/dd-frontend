import { action, Action, computed, Computed } from 'easy-peasy';

import { IToaster } from '../../../types/toaster';

type TToasterPayload = Omit<IToaster, 'timestamp'>;

interface IGlobalPreloader {
  isShown: boolean;
  message?: string;
  messageCode?: string;
  requestName?: string;
}

export interface IGlobalTechModel {
  toastersList: IToaster[];
  toasterTimestampsList: Computed<IGlobalTechModel, number[]>;
  addToaster: Action<IGlobalTechModel, TToasterPayload>;
  removeToaster: Action<IGlobalTechModel, number>;
  clearTosters: Action<IGlobalTechModel>;
  globalPreloaderParams: IGlobalPreloader;
  showGlobalPreloader: Action<
    IGlobalTechModel,
    Omit<IGlobalPreloader, 'isShown'>
  >;
  hideGlobalPreloader: Action<IGlobalTechModel>;
}

const globalTechModel: IGlobalTechModel = {
  toastersList: [],
  toasterTimestampsList: computed(state =>
    state.toastersList.map(toaster => toaster.timestamp),
  ),
  addToaster: action((state, payload) => {
    if (state.toastersList.length < 3) {
      state.toastersList.push({ ...payload, timestamp: Date.now() });
    } else {
      state.toastersList.pop();
      state.toastersList.push({ ...payload, timestamp: Date.now() });
    }
  }),
  removeToaster: action((state, payload) => {
    state.toastersList = state.toastersList.filter(
      toaster => toaster.timestamp !== payload,
    );
  }),
  clearTosters: action(state => {
    state.toastersList = [];
  }),
  globalPreloaderParams: { isShown: false },
  showGlobalPreloader: action((state, payload) => {
    state.globalPreloaderParams = {
      isShown: true,
      message: payload?.message,
      messageCode: payload.messageCode,
      requestName: payload?.requestName || undefined,
    };
  }),
  hideGlobalPreloader: action(state => {
    state.globalPreloaderParams = {
      isShown: false,
    };
  }),
};

export { globalTechModel };
