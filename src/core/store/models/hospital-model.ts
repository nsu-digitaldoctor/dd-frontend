import { action, Action } from 'easy-peasy';
import { ESpecialization } from '../../../types';

export interface ICompanyDoctor {
  id: number;
  fullName: string;
  rating?: number;
  price: number;
  specialization?: ESpecialization;
  totalReviews?: number;
  hospital: string;
}

export interface IHospitalModel {
  id: number;
  setId: Action<IHospitalModel, number>;
  IFT: string;
  setIFT: Action<IHospitalModel, string>;
  name: string; // "Central Hospital"
  setName: Action<IHospitalModel, string>
  email: string;
  setEmail: Action<IHospitalModel, string>;
  address: string; // "123 Health St."
  setAddress: Action<IHospitalModel, string>;
  rating: number; // 4.5
  setRating: Action<IHospitalModel, number>;
  description: string; // "A leading healthcare provider..."
  setDescription: Action<IHospitalModel, string>;
  fullName: string; // "123 Health St., Central Hospital"
  setFullName: Action<IHospitalModel, string>;
  doctors: ICompanyDoctor[];
  setHospitalDoctors: Action<IHospitalModel, ICompanyDoctor[]>;
}

const hospitalModel: IHospitalModel = {
  id: -1,
  setId: action((state, payload) => {
    state.id = payload;
  }),
  IFT: '',
  setIFT: action((state, payload) => {
    state.IFT = payload;
  }),
  email: '',
  setEmail: action((state, payload) => {
    state.email = payload;
  }),
  name: '',
  setName: action((state, payload) => {
    state.name = payload
  }),
  rating: -1,
  setRating: action((state, payload) => {
    state.rating = payload;
  }),
  address: '',
  setAddress: action((state, payload) => {
    state.address = payload;
  }),
  description: '',
  setDescription: action((state, payload) => {
    state.description = payload;
  }),
  fullName: '',
  setFullName: action((state, payload) => {
    state.fullName = payload;
  }),
  doctors: [],
  setHospitalDoctors: action((state, payload) => {
    state.doctors = payload;
  }),
};

export { hospitalModel };
