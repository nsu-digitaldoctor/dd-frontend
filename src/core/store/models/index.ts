export * from './global-model';
export * from "./popup-model";
export * from './global-tech-model';
export * from './user-model';
export * from './chat-model';
export * from './hospital-model';
export * from './appointment-model';
