import { action, Action, computed, Computed } from 'easy-peasy';
import { IPopup } from '../../../types';


export interface IPopupModel {
  isOpen: Computed<IPopupModel, boolean>;
  open: Action<IPopupModel, IPopup>;
  close: Action<IPopupModel>;
  popups: IPopup[];
}

// каждая новая модалка будет добавляться в очередь и выводиться поверх предыдущих
const popupModel: IPopupModel = {
  popups: [],
  isOpen: computed(state => state.popups.length > 0),

  open: action((state, payload) => {
    state.popups.push({
      render: payload.render,
      type: payload.type,
      withBackground: payload.withBackground,
      shouldHidePreviousPopups: payload.shouldHidePreviousPopups,
      size: payload.size,
      position: payload.position ?? 'top',
      onClose: payload.onClose,
    });
  }),

  close: action(state => {
    state.popups.pop();
  }),
};

export { popupModel };
