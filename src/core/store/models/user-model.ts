import { action, Action, thunk, Thunk } from 'easy-peasy';
import { EUserRole } from '../../../types';
import { IDocument } from '../../../types/documents';
import { IStore } from '../store';

export interface IUserName {
  firstName?: string;
  lastName?: string;
  middleName?: string;
}

export interface IUserModel {
  id: number;
  setId: Action<IUserModel, number>;
  userName: IUserName;
  setUserName: Action<IUserModel, IUserName>
  role: EUserRole;
  setRole: Action<IUserModel, EUserRole>
  email: string;
  setEmail: Action<IUserModel, string>;
  documents: IDocument[];
  setDocuments: Action<IUserModel, IDocument[]>;
  addDocument: Action<IUserModel, IDocument>;
  accessToken?: string;
  setAccessToken: Action<IUserModel, string>;
  refreshToken?: string;
  setRefreshToken: Action<IUserModel, string>;
  onUpdateTokens: Thunk<
    IUserModel, 
    never,
    never, 
    IStore
  >; 
}

const userModel: IUserModel = {
  id: -1,
  setId: action((state, payload) => {
    state.id = payload;
  }),
  email: '',
  setEmail: action((state, payload) => {
    state.email = payload;
  }),
  userName: {},
  setUserName: action((state, payload) => {
    state.userName = payload
  }),
  role: EUserRole.USER,
  setRole: action((state, payload) => {
    state.role = payload
  }),
  documents: [],
  setDocuments: action((state, payload) => {
    state.documents = payload
  }),
  addDocument: action((state, payload) => {
    state.documents.push(payload)
  }),
  setAccessToken: action((state, payload) => {
    state.accessToken = payload;
  }),
  setRefreshToken: action((state, payload) => {
    state.refreshToken = payload;
  }),
  onUpdateTokens: thunk(
    async (actions, _, { getState, getStoreState }) => {
      const apiClient = getStoreState().global.apiClient;
      const refreshToken = getState().refreshToken;

      if (apiClient !== undefined && refreshToken !== undefined) {
        const response = await apiClient.refreshToken({refreshToken})

        actions.setAccessToken(response.accessToken);
      }
    }),
};

export { userModel };
