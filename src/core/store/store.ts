import { createStore } from 'easy-peasy';
import { 
  appointmentModel,
  chatModel,
  globalModel, 
  globalTechModel, 
  hospitalModel, 
  IAppointmentModel, 
  IChatModel, 
  IGlobalModel, 
  IGlobalTechModel, 
  IHospitalModel, 
  IPopupModel, 
  IUserModel, 
  popupModel, 
  userModel
} from './models';
export interface IStore {
    global: IGlobalModel;
    popupModel: IPopupModel;
    globalTechModel: IGlobalTechModel;
    userModel: IUserModel;
    chatModel: IChatModel;
    hospitalModel: IHospitalModel;
    appointmentModel: IAppointmentModel;
}

const storeModel: IStore = {
    global: globalModel,
    popupModel,
    globalTechModel,
    userModel,
    chatModel,
    hospitalModel,
    appointmentModel,
}

const store = createStore<IStore>(storeModel);

export default store;
export { storeModel };