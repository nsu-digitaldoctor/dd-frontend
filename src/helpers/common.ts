export const capitalize = (str: string): string =>
  str
    .split(' ')
    .map(item => `${item.charAt(0).toUpperCase()}${item.substring(1)}`)
    .join(' ');