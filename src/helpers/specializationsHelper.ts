import { ESpecialization } from "../types";

export const getValueByKeyForSpecializationEnum = (value: string) => {
    return Object.entries(ESpecialization).find(([key, val]) => key === value)?.[1];
}