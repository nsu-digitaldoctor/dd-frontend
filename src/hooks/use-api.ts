import { useContext } from 'react';

import { ApiContext } from '../core/contexts/ApiContext';

export const useApi = () => useContext(ApiContext);
