import { useContext } from 'react';

import { LocaleContext } from '../core/contexts/LocaleContext';
import { TLocale } from '../i18nConfig';

export const useLocale = (): TLocale => useContext(LocaleContext);
