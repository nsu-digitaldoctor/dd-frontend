import { i18n as II18n, TFunction } from 'i18next';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import '../i18nConfig';
import { TNameSpace } from '../i18nConfig';

import { useStoreState } from './typed-store-hooks';

// eslint-disable-next-line import/no-anonymous-default-export
export default (ns?: TNameSpace): { t: TFunction<TNameSpace>; i18n: II18n } => {
  const localeFromStore = useStoreState(state => state.global.appLocale);

  const appLocale = localeFromStore;

  const { t, i18n } = useTranslation(ns);

  const isLanguageSwitched = i18n.language !== appLocale;

  useEffect(() => {
    if (isLanguageSwitched) {
      i18n.changeLanguage(appLocale);
    }
  }, [appLocale, i18n, isLanguageSwitched]);

  return { t, i18n };
};
