import { useCallback, useMemo } from "react";
import { useNavigate } from "react-router-dom";

export const routes = {
    userLoginPage: "/auth/login/user",
    userRegisterPage: "/auth/register/user",
    doctorLoginPage: "/auth/login/doctor",
    companyLoginPage: "/auth/login/company",
    companyRegisterPage: "/auth/register/company",
    landingPage: "/",
    userMainPage: "/user/doctors",
    userAppointments: "/user/appointments",
    userInfo: "/user/account",
    chats: "/chats",
    hospitalsDoctorPage: "/hospital/doctors",
    hospitalCabinet: '/hospital/cabinet',
    currentDoctorAppointments: '/doctor/appointments/current',
    doneDoctorAppointments: '/doctor/appointments/done',
}

export function useRoute(){
    const navigate = useNavigate();

    const navigateToUserLoginPage = useCallback(() => {
        navigate(routes.userLoginPage);
    }, [navigate]);

    const navigateToUserRegisterPage = useCallback(() => {
        navigate(routes.userRegisterPage);
    }, [navigate]);

    const navigateToDoctorLoginPage = useCallback(() => {
        navigate(routes.doctorLoginPage);
    }, [navigate]);

    const navigateToCompanyLoginPage = useCallback(() => {
        navigate(routes.companyLoginPage);
    }, [navigate]);

    const navigateToCompanyRegisterPage = useCallback(() => {
        navigate(routes.companyRegisterPage);
    }, [navigate]);

    const navigateToLandingPage = useCallback(() => {
        navigate(routes.landingPage);
    }, [navigate]);

    const navigateToUserMainPage = useCallback(() => {
        navigate(routes.userMainPage);
    }, [navigate]);

    const navigateToUserAppointments = useCallback(() => {
        navigate(routes.userAppointments);
    }, []);

    const navigateToUserInfo = useCallback(() => {
        navigate(routes.userInfo);
    }, []);

    const navigateToChats = useCallback(() => {
        navigate(routes.chats);
    }, [navigate]);

    const navigateToHospitalsDoctors = useCallback(() => {
        navigate(routes.hospitalsDoctorPage);
    }, [navigate]);

    const navigateToHospitalCabinet = useCallback(() => {
        navigate(routes.hospitalCabinet);
    }, [navigate]);

    const navigateToCurrentDoctorAppointments = useCallback(() => {
        navigate(routes.currentDoctorAppointments);
    }, [navigate]);

    const navigateToDoneDoctorAppointments = useCallback(() => {
        navigate(routes.doneDoctorAppointments);
    }, [navigate]);

    return useMemo(() => {
        return{
            toUserLoginPage: navigateToUserLoginPage,
            toUserRegisterPage: navigateToUserRegisterPage,
            toDoctorLoginPage: navigateToDoctorLoginPage,
            toCompanyLoginPage: navigateToCompanyLoginPage,
            toCompanyRegisterPage: navigateToCompanyRegisterPage,
            toLandingPage: navigateToLandingPage,
            toUserMainPage: navigateToUserMainPage,
            toUserAppointments: navigateToUserAppointments,
            toUserInfo: navigateToUserInfo,
            toChats: navigateToChats,
            toHospitalDoctors: navigateToHospitalsDoctors,
            toHospitalCabinet: navigateToHospitalCabinet,
            toCurrentDoctorAppointments: navigateToCurrentDoctorAppointments,
            toDoneDoctorAppointments: navigateToDoneDoctorAppointments,
        }
    }, [navigateToUserLoginPage, navigateToUserRegisterPage, 
        navigateToDoctorLoginPage, navigateToUserAppointments, navigateToUserInfo,
        navigateToLandingPage, navigateToCompanyLoginPage, 
        navigateToCompanyRegisterPage, navigateToChats, navigateToUserMainPage,
        navigateToCompanyRegisterPage, navigateToChats, 
        navigateToHospitalsDoctors, navigateToHospitalCabinet]);
}