import i18n, { InitOptions } from 'i18next';
import intervalPlural from 'i18next-intervalplural-postprocessor';
import { initReactI18next } from 'react-i18next';

import enGlobal from './locales/global-en-EN';
import ruGlobal from './locales/global-ru-RU';
import apiEn from './locales/api/en-EN';
import apiRu from './locales/api/ru-RU';
import messagesEn from './locales/toaster-messages/en-EN';
import messagesRu from './locales/toaster-messages/ru-RU';


export const LOCALES = ['ru', 'en'] as const;
export type TLocale = (typeof LOCALES)[number];
export type TNameSpace =
  | 'common'
  | 'api'
  | 'toaster-messages';

interface IInitParams extends InitOptions {
  ns: TNameSpace[];
  defaultNS: TNameSpace;
  resources: {
    [keyLocales in TLocale]: {
      [keyNS in TNameSpace]: Record<string, unknown>;
    };
  };
  lng: TLocale;
  fallbackLng: TLocale;
}

const initParams: IInitParams = {
  ns: ['common', 'api', 'toaster-messages'],
  defaultNS: 'common',
  resources: {
    en: {
      common: enGlobal,
      api: apiEn,
      'toaster-messages': messagesEn,
    },
    ru: {
      common: ruGlobal,
      api: apiRu,
      'toaster-messages': messagesRu
    },
  },
  lng: 'ru',
  fallbackLng: 'en',
};

i18n.use(intervalPlural).use(initReactI18next).init(initParams);

export default i18n;
