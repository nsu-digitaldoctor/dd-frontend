import ReactDOM from 'react-dom/client';
import 'core-js/actual/promise/all-settled';

import App from './App';
import React from 'react';

ReactDOM.createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
      <App />
    </React.StrictMode>,
  )
