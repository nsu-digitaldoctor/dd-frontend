export default {
  add: 'Add',
  cancel: 'Cancel',
  continue: 'Continue',
  attention: 'Attention',
  next: 'Next',
  done: 'Done',
  save: 'Save',
  update: 'Update',
  ok: 'OK',
  close: 'Close',
  logout: 'Logout',
  fullName: 'Full name',
  phone: 'Telephone'
};