export default {
  add: 'Добавить',
  cancel: 'Отмена',
  continue: 'Продолжить',
  attention: 'Внимание',
  next: 'Далее',
  done: 'Готово',
  save: 'Сохранить',
  update: 'Изменить',
  ok: 'OK',
  close: 'Закрыть',
  logout: 'Выйти',
  fullName: 'ФИО',
  phone: 'Телефон'
};
