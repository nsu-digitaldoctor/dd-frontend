export default {
  request: {
    notFound: 'Requested resource was not found',
  },
};