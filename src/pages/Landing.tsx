import React from 'react';
import { useRoute } from '../hooks/useRoute';

export function Landing() {
    const routes = useRoute();

    return (
        <div>
            <h1>Digital Doctor</h1>
            <h3>temporary view</h3>
            <button onClick={routes.toUserLoginPage}>To Login Page</button>
            <button onClick={routes.toUserRegisterPage}>To Register Page</button>
        </div>
    );
}