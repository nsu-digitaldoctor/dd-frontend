import CssBaseline from '@mui/material/CssBaseline';
import {Box, Button, Grid, Paper, Typography} from '@mui/material';
import PermIdentityOutlinedIcon from '@mui/icons-material/PermIdentityOutlined';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import { FC, ReactElement, ReactNode, useCallback, useEffect, useState} from 'react';
import { useRoute } from '../hooks/useRoute';
import { useApi } from '../hooks/use-api';
import { useStoreActions, useStoreState } from '../hooks';
import { isAxiosError } from 'axios';
import { EUserRole } from '../types';
import { UserRegister } from './auth/UserRegister';
import { DoctorRegister } from './auth/DoctorRegister';

export function SideMenuWrapper(props: SideMenuWrapperProps) {
  const route = useRoute();
  const api = useApi();
  const loggedRole = localStorage.getItem("loggedRole");
  const {setId: setUserId, setUserName, setRole} = useStoreActions(state => state.userModel);
  const {setId: setOrgId, setAddress, setDescription, setName, setFullName} = useStoreActions(actions => actions.hospitalModel)
  const open = useStoreActions(actions => actions.popupModel.open);
  const {userName, role} = useStoreState(state => state.userModel);
  const { name: companyName } = useStoreState(state => state.hospitalModel);
  const [isLoaded, setIsLoaded] = useState(false);

  const logout = useCallback(() => {
    localStorage.setItem("loggedRole", "");
    localStorage.setItem("userId", "");
    localStorage.setItem("accessToken", "");
    localStorage.setItem("refreshToken", "");
    switch(loggedRole) {
      case EUserRole.DOCTOR: 
        route.toDoctorLoginPage();
        break;
      case EUserRole.USER:
        route.toUserLoginPage();
        break;
      case EUserRole.ORG:
        route.toCompanyLoginPage();
        break;
      default:
        route.toUserLoginPage();
    }
  }, [loggedRole, route]);

  const RouteButtons: FC = () => {
    // в онклик перенаправлять на другие страницы, используя useRoute
    switch(loggedRole) {
      case EUserRole.USER:
        return(
        <>
          <MenuItem text="Запись на прием" onClick={route.toUserMainPage}/>
          <MenuItem text="Документы" onClick={() => {}}/>
          <MenuItem text="Приемы" onClick={route.toUserAppointments}/>
          <MenuItem text="Чаты" onClick={route.toChats}/>
        </>)
      case EUserRole.DOCTOR:
        return (
        <>
          <MenuItem text="Текущие приемы" onClick={route.toCurrentDoctorAppointments}/>
          <MenuItem text="Прошедшие приемы" onClick={route.toDoneDoctorAppointments}/>
          <MenuItem text="Чаты" onClick={route.toChats}/>
        </>)
      case EUserRole.ORG:
        return (
        <>
          <MenuItem text="Список врачей" onClick={route.toHospitalDoctors}/>
          <MenuItem text="Зарегистрировать врача" onClick={() => 
          open({
            render: () => (
              <DoctorRegister/>
            ),
        })}/>
        </>)
      default: 
        return null;
    }
  }

  const HelpButtons = () => {
    switch(loggedRole) {
      case EUserRole.USER:
        return(
        <>
          <BottomMenuItem text={userName.firstName + " " + userName.lastName} onClick={route.toUserInfo} icon={<PermIdentityOutlinedIcon/>}/>
          <BottomMenuItem text='Помощь' onClick={() => {}} icon={<InfoOutlinedIcon/>}/>
          <BottomMenuItem text='Выйти' onClick={logout} icon={<LogoutOutlinedIcon/>}/>
        </>)
      case EUserRole.DOCTOR:
        return (
        <>
          <BottomMenuItem text={userName.firstName + " " + userName.lastName} onClick={() => {}} icon={<PermIdentityOutlinedIcon/>}/>
          <BottomMenuItem text='Помощь' onClick={() => {}} icon={<InfoOutlinedIcon/>}/>
          <BottomMenuItem text='Выйти' onClick={logout} icon={<LogoutOutlinedIcon/>}/>
        </>)
      case EUserRole.ORG:
        return (
        <>
          <BottomMenuItem text={companyName} onClick={route.toHospitalCabinet} icon={<PermIdentityOutlinedIcon/>}/>
          <BottomMenuItem text='Помощь' onClick={() => {}} icon={<InfoOutlinedIcon/>}/>
          <BottomMenuItem text='Выйти' onClick={logout} icon={<LogoutOutlinedIcon/>}/>
        </>)
      default: 
        return null;
    }
  }

  useEffect(() => {
    const userId = localStorage.getItem("userId");
    if (!userId) {
      return;
    }
    const isOrg = loggedRole === EUserRole.ORG;
    if (isOrg) {
      api.fetchHospital(+userId).then(response => {
        if(isAxiosError(response)){
          logout();
          return;
        }
        setOrgId(response.id);
        setName(response.name);
        setFullName(response.fullName)
        setAddress(response.address)
        setDescription(response.description)
        setRole(EUserRole.ORG);
        setIsLoaded(true);
      });
      return;
    }
    api.account().then(response => {
      if(isAxiosError(response)){
        logout();
        return;
      }
      if(loggedRole !== response.role){
        logout();
        return;
      }
      setUserId(response.id);
      setUserName({
        firstName: response.firstName,
        lastName: response.lastName,
        middleName: response.middleName
      });
      setRole(response.role as EUserRole);
      setIsLoaded(true);
    });
  }, [api, setUserId, setUserName, setRole, loggedRole, logout, 
    setOrgId, setName, setFullName, setAddress, setDescription]);

  return (
    <>
    {isLoaded &&
    <Grid container component="main" sx={{height: "100vh"}}>
      <CssBaseline />
      <Grid
        item
        md={2}
        component={Paper}
        elevation={2}
      >
        <Box sx={{
          my: 3,
          mx: 3,
          height: 50,
          width: "100%",
          backgroundImage: `url(${require("../images/logo.png")})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: '55%'
        }}/>
        <div style={{height: 'calc(100vh - 98px)', display: 'flex', flexDirection: 'column', justifyContent: 'space-between'}}>
          <Box
            sx={{
              mx: 5,
              display: "flex",
              flexDirection: 'column',
              justifyContent: "flex-start"
            }}
          >
            <RouteButtons />
          </Box>
          <Box
            sx={{
              mx: 5,
              display: "flex",
              flexDirection: 'column',
              justifyContent: "top"
            }}
          >
          <HelpButtons />
          </Box>
        </div>
      </Grid>
      <Grid
        item
        md={10}
      >
        {props.children}
      </Grid>
    </Grid>
    }
    </>
  );
}

function MenuItem(props: MenuItemProps){
  return(
    <Button
      onClick={props.onClick}
      sx={{
        my: 1,
        justifyContent: "start",
        borderRadius: 15,
        backgroundColor: "white",
        padding: 2,
        ':hover':{
            bgcolor: '#E5F1FF'
        },
        textTransform: "none"
      }}
      color="inherit"
    >
      <Typography
        variant='body1'  
        sx={{
          fontWeight: "bold",
          fontSize: 12
        }}
      >
        {props.text}
      </Typography>
    </Button>
  )
}

function BottomMenuItem(props: BottomMenuItemProps){
  return(
    <Button
      onClick={props.onClick}
      sx={{
        my: 1,
        justifyContent: "start",
        borderRadius: 15,
        padding: 2,
        textTransform: "none"
      }}
      color="inherit"
      startIcon={props.icon}
    >
      <Typography
        variant='body1'  
        sx={{
          fontWeight: "bold",
          fontSize: 12,
          color: "gray"
        }}
      >
        {props.text}
      </Typography>
    </Button>
    
  );
}

type MenuItemProps = {
  text: string, 
  onClick: () => void
}

type BottomMenuItemProps = {
  text: string, 
  onClick: () => void,
  icon: ReactNode
}

type SideMenuWrapperProps = {
  children: string | number | boolean | ReactElement | Iterable<ReactNode> | null | undefined
}
