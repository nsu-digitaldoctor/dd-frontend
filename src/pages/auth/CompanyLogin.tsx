import { useCallback, useEffect, useState } from 'react';
import { Box, Button, Checkbox, CssBaseline, FormControlLabel, Grid, Link, Paper, Typography} from '@mui/material';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import { routes, useRoute } from '../../hooks/useRoute';
import { InputField } from '../../components/InputField';
import { useApi } from '../../hooks/use-api';
import { isAxiosError } from 'axios';
import { useStoreActions } from '../../hooks';
import { EUserRole } from '../../types';

export function CompanyLogin() {
    const [IFT, setIFT] = useState("");
    const [password, setPassword] = useState("");
    const [rememberMe, setRememberMe] = useState(false);
    const [buttonActive, setButtonActive] = useState(false);
    const api = useApi();
    const route = useRoute();

    const open = useStoreActions(actions => actions.popupModel.open);
    const {setAccessToken, setRefreshToken} = useStoreActions(actions => {
      return actions.userModel;
    });
    const setId = useStoreActions(actions => actions.hospitalModel.setId);

    const submitLogin = useCallback(() => {
        api.loginHospital({inn: IFT, password}).then((response) => {
          if(isAxiosError(response)){
            console.log(response);
            if(response.response?.status === 401){
              open({
                render: () => (
                  <Typography
                  variant='body1'
                  sx={{
                    mx: 5,
                    my: 3
                  }}>
                    Неверная почта или пароль
                  </Typography>
                )
              })
            }else{
              open({
                render: () => (
                  <Typography
                  variant='body1'
                  sx={{
                    mx: 5,
                    my: 3
                  }}>
                    Сервер недоступен
                  </Typography>
                )
              })
            }
            return;
          }
          localStorage.setItem("loggedRole", EUserRole.ORG);
          localStorage.setItem("userId", response.id.toString());
          localStorage.setItem("accessToken", response.accessToken);
          localStorage.setItem("refreshToken", response.refreshToken);
          setAccessToken(response.accessToken);
          setRefreshToken(response.refreshToken);
          setId(response.id);
          route.toHospitalDoctors();
        }).catch((e) => {
          console.log(e);
        });
    }, [api, IFT, password, setAccessToken, setRefreshToken, setId, route, open]);

    const invertRememberMe = useCallback(() => {
      setRememberMe(!rememberMe);
    }, [rememberMe]);

    useEffect(() => {
        setButtonActive(password.length >= 8);
    }, [password])

    return (
    <div>
      <Grid container component="main" sx={{ height: '100vh' }}>
        <CssBaseline />
        <Grid
          item
          md={8}
          sx={{
            backgroundImage: `url(${require("../../images/background.png")})`,
            backgroundRepeat: 'no-repeat',
            backgroundColor: "#0C5AB2",
            backgroundSize: 'cover',
          }}
        />
        <Grid item md={4} component={Paper} elevation={6}>
          <Button sx={{
            my: 2,
            mx: 2
          }}
            startIcon={<AccountCircleOutlinedIcon/>}
            onClick={route.toUserLoginPage}>
            Вход для физических лиц
          </Button>
          <Button sx={{
            my: 2,
            mx: 2
          }}
            startIcon={<AccountCircleOutlinedIcon/>}
            onClick={route.toDoctorLoginPage}>
            Вход для врачей
          </Button>
          <Box
            sx={{
              my: 8,
              mx: 8,
              display: 'flex',
              flexDirection: 'column',
              justifyContent: "center",
              minHeight: "65vh"
            }}
          >
            <Typography component="h1" variant="h4">
              Вход
            </Typography>
            <Box component="form" noValidate sx={{ mt: 1 }}>
              <InputField.Input
                  required
                  value={IFT}
                  label="ИНН"
                  onChange={setIFT}
                  autoFocus
              />
              <InputField.Password
                required
                value={password}
                label="Пароль"
                onChange={setPassword}
                autoComplete='password'
              />
              <FormControlLabel
                control={<Checkbox value={rememberMe} onChange={invertRememberMe} color="primary"/>}
                label="Не выходить"
              />
              <Button
                onClick={submitLogin}
                fullWidth
                disabled={!buttonActive}
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Войти
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link href="#" variant="body2">
                    Забыли пароль?
                  </Link>
                </Grid>
                <Grid item>
                  <Link href={routes.companyRegisterPage} variant="body2">
                    {"Впервые здесь?"}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
    );
}