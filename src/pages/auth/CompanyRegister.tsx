import { Box, Button, CssBaseline, Grid, Link, Paper, Typography } from '@mui/material';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import { routes, useRoute } from '../../hooks/useRoute';
import { useCallback, useEffect, useState } from 'react';
import { InputField } from '../../components/InputField';
import { checkEmail, checkPassword } from '../../helpers/credentialsCheckers';
import { useApi } from '../../hooks/use-api';
import { useStoreActions } from '../../hooks';

export function CompanyRegister() {
    const open = useStoreActions(actions => actions.popupModel.open);
    const [name, setName] = useState('');
    const [IFT, setIFT] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [passwordsMatch, setPasswordsMatch] = useState(PasswordMatch.empty);
    const api = useApi();
    const route = useRoute();

    const comparePasswords = useCallback(() : PasswordMatch => {
        if(password === "" || repeatPassword === ""){
            return PasswordMatch.empty;
        }else{
            return password === repeatPassword ? PasswordMatch.match : PasswordMatch.notMatch;
        }
    }, [password, repeatPassword]);

    const submitRegister = useCallback(() => {
        api.registerHospital({name, inn: IFT, email, password})
        .then(() => {
          route.toCompanyLoginPage();
        }).catch((e) => {
          open({
            render: () => (
              <Typography>
                Не удалось зарегистрировать клинику
              </Typography>
            )
          })
        });
    }, [api, name, IFT, email, password, route, open]);

    useEffect(() => {
        setPasswordsMatch(comparePasswords());
    }, [password, repeatPassword, comparePasswords]);

    return (
        <div>
          <Grid container component="main" sx={{ height: '100vh' }}>
            <CssBaseline />
            <Grid
              item
              md={8}
              sx={{
                backgroundImage: `url(${require("../../images/background.png")})`,
                backgroundRepeat: 'no-repeat',
                backgroundColor: "#0C5AB2",
                backgroundSize: 'cover',
              }}
            />
            <Grid item md={4} component={Paper} elevation={6}>
              <Button sx={{
                my: 2,
                mx: 2
              }}
                startIcon={<AccountCircleOutlinedIcon/>}
                onClick={route.toUserRegisterPage}>
                Регистрация для физических лиц
              </Button>
              <Box
                sx={{
                  my: 8,
                  mx: 8,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: "center",
                  minHeight: "75vh"
                }}
              >
                <Typography component="h1" variant="h4">
                  Регистрация
                </Typography>
                <Box component="form" noValidate sx={{ mt: 1 }}>
                  <InputField.Input
                    required
                    value={name}
                    label="Название компании"
                    onChange={setName}
                    autoFocus
                  />
                  <InputField.Input
                    required
                    value={IFT}
                    label="ИНН"
                    onChange={setIFT}
                    errorHandler={{
                      errorText: "Длина ИНН должна быть 10 символов",
                      errorChecker: (val: string) => val.length === 10
                    }}
                  />
                  <InputField.Input
                    required
                    value={email}
                    label="Почта"
                    onChange={setEmail}
                    errorHandler={{
                        errorText: "Неправильный формат почты",
                        errorChecker: checkEmail
                    }}
                  />
                  <InputField.Password
                    required
                    value={password}
                    label="Пароль"
                    onChange={setPassword}
                    errorHandler={{
                      errorText: "Пароль должен быть не короче 8 символов, содержать хотя бы одну заглавную букву, цифру и спец. символ",
                      errorChecker: checkPassword
                    }}
                  />
                  <InputField.Password
                    required
                    value={repeatPassword}
                    label="Повторите пароль"
                    onChange={setRepeatPassword}
                  />
                  <Typography color={"#D32F2F"} textAlign={"center"}>
                    {passwordsMatch === PasswordMatch.notMatch && "Пароли не совпадают"}
                  </Typography>
                  <Button
                    onClick={submitRegister}
                    fullWidth
                    disabled={!(comparePasswords() === PasswordMatch.match && checkEmail(email))}
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Зарегистрироваться
                  </Button>
                  <Grid container>
                    <Grid item>
                      <Link href={routes.companyLoginPage} variant="body2">
                        {"Уже зарегистрированы?"}
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </div>
    );
}

enum PasswordMatch {
    empty,
    match,
    notMatch
}