import { Box, Button, FormControl, Grid, InputLabel, MenuItem, Select, SelectChangeEvent, Typography } from '@mui/material';
import { useCallback, useEffect, useState } from 'react';
import { InputField } from '../../components/InputField';
import { checkEmail, checkPassword } from '../../helpers/credentialsCheckers';
import { useApi } from '../../hooks/use-api';
import { useStoreActions, useStoreState } from '../../hooks';
import { ESpecialization } from '../../types';

export function DoctorRegister() {
    const open = useStoreActions(actions => actions.popupModel.open);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [middleName, setMiddleName] = useState("");
    const [spec, setSpec] = useState('');
    const [email, setEmail] = useState("");
    const [price, setPrice] = useState('');
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [passwordsMatch, setPasswordsMatch] = useState(PasswordMatch.empty);
    const api = useApi();
    const close = useStoreActions(actions => actions.popupModel.close);
    const id = useStoreState(state => state.hospitalModel.id);
    const setDoctors = useStoreActions(actions => actions.hospitalModel.setHospitalDoctors);

    const handleChangeSpec = (event: SelectChangeEvent) => {
      setSpec(event.target.value);
    };

    const comparePasswords = useCallback(() : PasswordMatch => {
        if(password === "" || repeatPassword === ""){
            return PasswordMatch.empty;
        }else{
            return password === repeatPassword ? PasswordMatch.match : PasswordMatch.notMatch;
        }
    }, [password, repeatPassword]);

    const submitRegister = useCallback(() => {
        api.registerHospitalDoctor({firstName, lastName, middleName, email, password, price: +price, specialty: spec})
        .then(response => {
          if (response.status === 200) {
            close();
            api.fetchHospitalDoctors(id).then(response => {
              setDoctors(response);
            })
          }
        })
        .catch((e) => {
          open({
            render: () => (
              <Typography>
                Не удалось зарегистрировать специалиста
              </Typography>
            )
          })
        });
    }, [api, firstName, lastName, middleName, email, password, price, spec, open]);

    useEffect(() => {
        setPasswordsMatch(comparePasswords());
    }, [password, repeatPassword, comparePasswords]);

    return (
      <>
        <Typography
          variant='h5'
          sx={{
              mt: '20px',
              ml: '20px',
              height: "4vh"
          }}
          fontWeight="bold"
        >
          Регистрация специалиста
        </Typography>
        <Grid container sx={{ margin: '5px 20px', maxWidth: '60vw'}} spacing={2}>
          <Grid item xs={6}>
            <InputField.Input
              required
              value={lastName}
              label="Фамилия"
              onChange={setLastName}
              autoFocus
              errorHandler={{
                errorText: "Фамилия не может быть пустой",
                errorChecker: (val: string) => {return val.length > 0}
              }}
            />
            <InputField.Input
              required
              value={firstName}
              label="Имя"
              onChange={setFirstName}
              errorHandler={{
                errorText: "Имя не может быть пустым",
                errorChecker: (val: string) => {return val.length > 0}
              }}
            />
            <InputField.Input
              value={middleName}
              label="Отчество"
              onChange={setMiddleName}
            />
            <FormControl variant="filled" sx={{ mt: '16px', mb: '8px' }} fullWidth>
              <InputLabel id="spec-select-label">Специализация врача</InputLabel>
              <Select
                labelId="spec-select-label"
                value={spec}
                onChange={handleChangeSpec}
              >
                <MenuItem value=""> <em>Специализация врача</em></MenuItem>
                {Object.entries(ESpecialization).map(([key, spec]) => 
                <MenuItem value={key} key={key}>{spec}</MenuItem>)}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={6}>
            <InputField.Input
              required
              value={email}
              label="Почта"
              onChange={setEmail}
              errorHandler={{
                  errorText: "Неправильный формат почты",
                  errorChecker: checkEmail
              }}
            />
            <InputField.Input
              required
              value={price}
              label="Цена услуги"
              onChange={setPrice}
              errorHandler={{
                errorText: "Цена услуги должна содержать только цифры",
                errorChecker: (val: string) => /^\d+$/.test(val)
              }}
            />
            <InputField.Password
              required
              value={password}
              label="Пароль"
              onChange={setPassword}
              errorHandler={{
                errorText: "Пароль должен быть не короче 8 символов, содержать хотя бы одну заглавную букву, цифру и спец. символ",
                errorChecker: checkPassword
              }}
            />
            <InputField.Password
              required
              value={repeatPassword}
              label="Повторите пароль"
              onChange={setRepeatPassword}
            />
            <Typography color={"#D32F2F"} textAlign={"center"}>
              {passwordsMatch === PasswordMatch.notMatch && "Пароли не совпадают"}
            </Typography>
          </Grid>
          <Button
            onClick={submitRegister}
            fullWidth
            disabled={!(comparePasswords() === PasswordMatch.match && checkEmail(email) && firstName.length > 0 && lastName.length > 0)}
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Зарегистрировать
          </Button>
        </Grid>
      </>
    );
}

enum PasswordMatch {
    empty,
    match,
    notMatch
}