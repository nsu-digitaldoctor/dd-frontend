import { Box, Button, CssBaseline, Grid, Link, Paper, Typography } from '@mui/material';
import ApartmentOutlinedIcon from '@mui/icons-material/ApartmentOutlined';
import { routes, useRoute } from '../../hooks/useRoute';
import { useCallback, useEffect, useState } from 'react';
import { InputField } from '../../components/InputField';
import { checkEmail, checkPassword } from '../../helpers/credentialsCheckers';
import { useApi } from '../../hooks/use-api';
import { useStoreActions } from '../../hooks';
import { EUserRole } from '../../types';

export function UserRegister() {
    const open = useStoreActions(actions => actions.popupModel.open);
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [middleName, setMiddleName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [passwordsMatch, setPasswordsMatch] = useState(PasswordMatch.empty);
    const api = useApi();
    const route = useRoute();

    const comparePasswords = useCallback(() : PasswordMatch => {
        if(password === "" || repeatPassword === ""){
            return PasswordMatch.empty;
        }else{
            return password === repeatPassword ? PasswordMatch.match : PasswordMatch.notMatch;
        }
    }, [password, repeatPassword]);

    const submitRegister = useCallback(() => {
        api.register(EUserRole.USER, {firstName, lastName, middleName, email, password})
        .then(() => {
          route.toUserLoginPage();
        }).catch((e) => {
          open({
            render: () => (
              <Typography>
                Не удалось зарегистрировать пользователя
              </Typography>
            )
          })
        });
    }, [email, password, api, firstName, lastName, middleName, open, route]);

    useEffect(() => {
        setPasswordsMatch(comparePasswords());
    }, [password, repeatPassword, comparePasswords]);

    return (
        <div>
          <Grid container component="main" sx={{ height: '100vh' }}>
            <CssBaseline />
            <Grid
              item
              md={8}
              sx={{
                backgroundImage: `url(${require("../../images/background.png")})`,
                backgroundRepeat: 'no-repeat',
                backgroundColor: "#0C5AB2",
                backgroundSize: 'cover',
              }}
            />
            <Grid item md={4} component={Paper} elevation={6}>
              <Button sx={{
                my: 2,
                mx: 2
              }}
                startIcon={<ApartmentOutlinedIcon/>}
                onClick={route.toCompanyRegisterPage}>
                Регистрация для компаний
              </Button>
              <Box
                sx={{
                  my: 8,
                  mx: 8,
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: "center",
                  minHeight: "75vh"
                }}
              >
                <Typography component="h1" variant="h4">
                  Регистрация
                </Typography>
                <Box component="form" noValidate sx={{ mt: 1 }}>
                  <InputField.Input
                    required
                    value={lastName}
                    label="Фамилия"
                    onChange={setLastName}
                    autoFocus
                    errorHandler={{
                      errorText: "Фамилия не может быть пустой",
                      errorChecker: (val: string) => {return val.length > 0}
                    }}
                  />
                  <InputField.Input
                    required
                    value={firstName}
                    label="Имя"
                    onChange={setFirstName}
                    errorHandler={{
                      errorText: "Имя не может быть пустым",
                      errorChecker: (val: string) => {return val.length > 0}
                    }}
                  />
                  <InputField.Input
                    value={middleName}
                    label="Отчество"
                    onChange={setMiddleName}
                  />
                  <InputField.Input
                    required
                    value={email}
                    label="Почта"
                    onChange={setEmail}
                    errorHandler={{
                        errorText: "Неправильный формат почты",
                        errorChecker: checkEmail
                    }}
                  />
                  <InputField.Password
                    required
                    value={password}
                    label="Пароль"
                    onChange={setPassword}
                    errorHandler={{
                      errorText: "Пароль должен быть не короче 8 символов, содержать хотя бы одну заглавную букву, цифру и спец. символ",
                      errorChecker: checkPassword
                    }}
                  />
                  <InputField.Password
                    required
                    value={repeatPassword}
                    label="Повторите пароль"
                    onChange={setRepeatPassword}
                  />
                  <Typography color={"#D32F2F"} textAlign={"center"}>
                    {passwordsMatch === PasswordMatch.notMatch && "Пароли не совпадают"}
                  </Typography>
                  <Button
                    onClick={submitRegister}
                    fullWidth
                    disabled={!(comparePasswords() === PasswordMatch.match && checkEmail(email) && firstName.length > 0 && lastName.length > 0)}
                    variant="contained"
                    sx={{ mt: 3, mb: 2 }}
                  >
                    Зарегистрироваться
                  </Button>
                  <Grid container>
                    <Grid item>
                      <Link href={routes.userLoginPage} variant="body2">
                        {"Уже зарегистрированы?"}
                      </Link>
                    </Grid>
                  </Grid>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </div>
    );
}

enum PasswordMatch {
    empty,
    match,
    notMatch
}