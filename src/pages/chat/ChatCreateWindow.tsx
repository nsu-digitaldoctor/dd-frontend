import { Box, Button, Checkbox, FormControlLabel, Typography } from "@mui/material";
import { InputField } from "../../components/InputField";
import { useCallback, useState } from "react";
import { useApi } from "../../hooks/use-api";
import { useStoreState } from "../../hooks";

export function ChatCreateWindow(){
    const api = useApi();
    const userId = useStoreState(state => state.userModel.id);
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [isAnonymous, setIsAnonymous] = useState(true);
    const [buttonDisabled, setButtonDisabled] = useState(true);

    const invertAnonymous = useCallback(() => {
        setIsAnonymous(!isAnonymous);
      }, [isAnonymous]);

    const checkTitle = useCallback((str: string) => {
        if(str.length >= 3){
            setButtonDisabled(false);
            return true;
        }else{
            setButtonDisabled(true);
            return false;
        }
    }, []);

    const createChat = useCallback(() => {
        api.createChat({
            chat: {
                title,
                isAnonymous
            },
            user: {
                id: userId
            }
        }).then(async(response) => {
            if(response && description.length > 0){
                await api.createMessage({
                    message: {
                        chatId: response.id,
                        content: description,
                        replyMessageId: null
                    },
                    user: {
                        id: userId
                    }
                });
            }
            window.location.reload();
        });
    }, [api, title, description, isAnonymous, userId]);

    return(
        <Box
            sx={{
                width: "100%",
                my: 2
            }}
        >
            <Box
                sx = {{
                    paddingBottom: "2ch",
                    paddingLeft: "2ch",
                    marginBottom: "2ch",
                    borderBottom: 1,
                    borderRadius: 0,
                    borderColor: "#ccc"
                }}
            >
                <Typography
                    variant='h4'
                    fontWeight="bold"
                >
                    Создать обращение
                </Typography>
            </Box>
            <Box
            sx={{
                paddingLeft: "2ch",
                width: "95%"
            }}
            >
                <InputField.Input
                    label="Тема"
                    required
                    value={title}
                    onChange={setTitle}
                    errorHandler={{
                        errorText: "Тема должна содержать не менее трех символов",
                        errorChecker: checkTitle
                    }}
                />
                <InputField.Text
                    label="Описание"
                    value={description}
                    onChange={setDescription}
                />
                <FormControlLabel
                    control={<Checkbox defaultChecked={isAnonymous} value={isAnonymous} onChange={invertAnonymous} color="primary"/>}
                    label="Анонимное обращение"
                />
            </Box>
            <Box
                sx={{
                    paddingLeft: "2ch",
                    paddingTop: "2ch",
                    width: "95%",
                    display: "flex",
                    justifyContent: "center"
                }}
            >
                <Button
                    onClick={createChat}
                    variant="outlined"
                    sx={{
                        borderRadius: 5
                    }}
                    disabled={buttonDisabled}
                >
                    Создать обращение
                </Button>
            </Box>
        </Box>
    );
};