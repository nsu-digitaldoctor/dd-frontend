import { Box, Button, Typography } from '@mui/material';
import { Dispatch, SetStateAction, useCallback } from 'react';
import { ChatInfo, ChatMessage, EChatStatus } from '../../types/chatTypes';
import { EUserRole } from '../../types';
import { useStoreActions, useStoreState } from '../../hooks';
import { useApi } from '../../hooks/use-api';

export function ChatMessageItem(props: ChatMessageItemProps) {
    const api = useApi();
    const {open, close} = useStoreActions(actions => actions.popupModel);
    const {id: userId, role} = useStoreState(state => state.userModel);
    const isYourMsg = props.message.user?.id === userId || (!props.message.user && role === EUserRole.USER);

    const deleteMessage = useCallback(() => {
        open({
            render: () => (
                <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    mx: 2,
                    my: 2,
                    width: "100%",
                    height: "30%"
                }}
                >
                    <Typography
                        variant='h6'
                        fontWeight="bold"
                        sx={{
                            width: "90%"
                        }}
                    >
                        Вы уверены, что хотите удалить сообщение?
                    </Typography>
                    <Box>
                        <Button
                            onClick={() => {
                                api.deleteMessage({
                                    message:{
                                        messageId: props.message.message_id
                                    },
                                    user: {
                                        id: userId
                                    }
                                }).then(() => {
                                    props.getMessages();
                                    close();
                                })
                            }}
                        >
                            Да
                        </Button>
                        <Button
                            onClick={() => {
                                close();
                            }}
                        >
                            Нет
                        </Button>
                    </Box>
                </Box>
            )
        })
        
    }, [api, userId, props.message, open, close, props.getMessages]);

    return (
        <Box
            sx={{
                width: "60%",
                my: 2,
                borderRadius: 2,
                borderColor: "#ccc",
                backgroundColor: "#eaeaea",
                padding: "2ch"
            }}
        >
            {props.replyMessage !== undefined &&
            <Box
                sx={{
                    width: "100%",
                    marginBottom: 2,
                    borderRadius: 2,
                    borderColor: "#ccc",
                    backgroundColor: "#bababa",
                    padding: "2ch"
            }}>
                {props.replyMessage !== null ?
                <>
                <Typography
                    variant="body2"
                    fontWeight="bold"
                >
                    {props.replyMessage.user ? props.replyMessage.user.lastName + " " + props.replyMessage.user.firstName : "Аноним"}
                </Typography>
                <Typography
                    noWrap
                    variant="body2"
                >
                    {props.replyMessage.content}
                </Typography>
                </>
                :
                <>
                <Typography
                    variant="body2"
                >
                    Сообщение удалено
                </Typography>
                </>
                }
            </Box>
            }
            <Box
            sx={{
                display: "flex",
                flexDirection: "row"
            }}>
                <Typography
                    variant="body2"
                    fontWeight="bold"
                    sx={{
                        width: "70%"
                    }}
                >
                    {props.chatInfo.isAnonymous ? 
                        ( props.message.user ? props.message.user?.lastName + " " + props.message.user?.firstName + 
                        (isYourMsg ? " (Вы)" : "") + " | Врач" : "Аноним (Вы)") 
                    : 
                        props.message.user?.lastName + " " + props.message.user?.firstName + 
                        (isYourMsg ? " (Вы)" : "")
                        + (props.message.user?.role === EUserRole.DOCTOR ? " | Врач" : "")}
                </Typography>
                {props.chatInfo.status !== EChatStatus.RESOLVED &&
                <>
                <Button
                    sx={{
                        width: isYourMsg? "15%" : "30%",
                        textTransform: "none",
                        padding: 0,
                        fontSize: 12
                    }}
                    onClick={() => {
                        props.setReplyMessage(props.message);
                    }}
                >
                    Ответить
                </Button>
                {isYourMsg && 
                <Button
                    sx={{
                        width: "15%",
                        padding: 0,
                        fontSize: 12,
                        textTransform: "none"
                    }}
                    color='error'
                    onClick={deleteMessage}
                >
                    Удалить
                </Button>
                }
                </>
                }
            </Box>
            <Typography
                whiteSpace="normal"
                variant="body2"
            >
                {props.message.content}
            </Typography>
            <Typography
                variant='body2'
                color= "gray"
                textAlign="right"
            >
                {props.message.sent_date.toLocaleDateString() + " " + props.message.sent_date.toLocaleTimeString()}
            </Typography>
        </Box>
    );
};

export type ChatMessageItemProps = {
    message: ChatMessage,
    replyMessage?: ChatMessage | null,
    chatInfo: ChatInfo,
    setReplyMessage: Dispatch<SetStateAction<ChatMessage | undefined>>,
    getMessages: () => void
}