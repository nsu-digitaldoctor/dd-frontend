import { Box, Button, Typography } from "@mui/material";
import { ChatInfo, ChatMessage, EChatStatus, messagesMapper } from "../../types/chatTypes";
import { Dispatch, ReactNode, SetStateAction, useCallback, useEffect, useState } from "react";
import SendOutlinedIcon from '@mui/icons-material/SendOutlined';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import { InputField } from "../../components/InputField";
import { useApi } from "../../hooks/use-api";
import { useStoreActions, useStoreState } from "../../hooks";
import { ChatResolvingWindow } from "./ChatResolvingWindow";
import { EUserRole } from "../../types";
import { ChatMessageItem } from "./ChatMessageItem";

export function ChatMessagesWindow(props: ChatMessagesWindowProps){
    const api = useApi();
    const {id: userId, role} = useStoreState(state => state.userModel);
    const open = useStoreActions(actions => actions.popupModel.open);
    const [message, setMessage] = useState("");
    const [messages, setChatMessages] = useState<ChatMessage[]>([]);
    const [replyMessage, setReplyMessage] = useState<ChatMessage | undefined>();

    const getMessages = useCallback(() => {
        if(props.chatInfo){
            api.getMessagesList({
                chat:{
                    id: props.chatInfo.id
                },
                user:{
                    id: userId !== null ? +userId : -1
                }
            }).then((response) => {
                setChatMessages(messagesMapper(response));
            })
        }
    }, [api, props.chatInfo, userId]);

    const setClosed = useCallback(() => {
        if(props.chatInfo){
            api.updateChat({
                chats: [{
                    chatId: props.chatInfo.id,
                    newStatus: EChatStatus.CLOSED,
                }],
                user: {
                    id: userId
                }
            }).then(() => {
                window.location.reload();
            })
        }
    }, [api, props.chatInfo, userId]);

    const setResolved = useCallback(() => {
        if(props.chatInfo && messages.length > 0){
            open({
                render: () => (
                    <ChatResolvingWindow chatInfo={props.chatInfo!} messages={messages}/>
                )
            })
            
        }
    }, [props.chatInfo, messages, open]);

    const setNotAnonymous = useCallback(() => {
        if(props.chatInfo){
            api.updateChat({
                chats: [{
                    chatId: props.chatInfo.id,
                    newIsAnonymous: false,
                }],
                user: {
                    id: userId
                }
            }).then(() => {
                window.location.reload();
            })
        }
    }, [api, props.chatInfo, userId]);

    const sendMessage = useCallback(() => {
        if(props.chatInfo && message.length > 0){
            api.createMessage({
                message: {
                    chatId: props.chatInfo.id,
                    content: message,
                    replyMessageId: replyMessage ? replyMessage.message_id : null
                },
                user: {
                    id: userId
                }
            }).then(() => {
                setMessage("");
                setReplyMessage(undefined);
                getMessages();
            });
        }
    }, [api, message, props.chatInfo, userId, getMessages, replyMessage]);

    useEffect(() => {
        setMessage("");
        setReplyMessage(undefined);
        setChatMessages([]);
        getMessages();
    }, [props.chatInfo, getMessages, setReplyMessage]);

    return(
        <>
        {props.chatInfo &&
        <Box
            sx={{
                width: "100%",
                marginTop: 2,
                heigth: "85vh"
            }}
        >
            <Box
            sx = {{
                paddingBottom: "1ch",
                paddingLeft: "2ch",
                display: "flex",
                flexDirection: "row"
            }}
            >
                <Typography
                    variant='h4'
                    fontWeight="bold"
                    sx={{
                        width: "80%"
                    }}
                >
                    {props.chatInfo.title}
                </Typography>
                {props.chatInfo.isAnonymous && role === EUserRole.USER &&
                        <Button
                        sx={{
                            textTransform: "none",
                            borderRadius: 5,
                            ':hover':{
                                bgcolor: '#E5F1FF'
                            },
                        }}
                        color="inherit"
                        onClick={setNotAnonymous}
                        >
                            Сделать не анонимным
                        </Button>
                }
                { (role === EUserRole.USER && props.chatInfo.status === EChatStatus.ANSWERED) &&
                <>
                    
                    <Button
                        sx={{
                            textTransform: "none",
                            borderRadius: 5,
                            ':hover':{
                                bgcolor: '#E5F1FF'
                            },
                        }}
                        color="inherit"
                        onClick={setClosed}
                    >
                        Не решено
                    </Button>
                    <Button
                        sx={{
                            textTransform: "none",
                            borderRadius: 5,
                            ':hover':{
                                bgcolor: '#E5F1FF'
                            },
                        }}
                        color="inherit"
                        onClick={setResolved}
                    >
                        Решено
                    </Button>
                </>
                }
            </Box>
            <Box
                sx={{
                    paddingLeft: "2ch",
                    paddingBottom: "2ch",
                    borderBottom: 1,
                    borderRadius: 0,
                    borderColor: "#ccc",
                    display: "flex",
                    flexDirection: "row"
                }}
            >
                {props.chatInfo.status === EChatStatus.CLOSED ?
                <>
                    <Typography
                        variant="body2"
                        color="#DC481A"
                        paddingRight={2}
                        fontWeight="bold"
                    >
                        Не решено
                    </Typography>
                </>
                :
                props.chatInfo.status === EChatStatus.RESOLVED &&
                <>
                    <Typography
                        variant="body2"
                        color="#0C5AB2"
                        paddingRight={2}
                        fontWeight="bold"
                    >
                        Решено врачем
                    </Typography>
                </>
                }
                <Typography
                    variant='body2'
                    color= "gray"
                    textAlign="left"
                >
                    {props.chatInfo.createDate.toLocaleDateString()}
                </Typography>
            </Box>
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    overflow: "auto",
                    height: replyMessage ? "66vh" : "78vh",
                    paddingLeft: "2ch"
                }}
            >
                    {showChatMessages(messages, props.chatInfo, setReplyMessage, getMessages)}
            </Box>
            {(props.chatInfo.status === EChatStatus.NEW || props.chatInfo.status === EChatStatus.ANSWERED) &&
            <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    paddingLeft: "2ch"
                }}
            >
                {replyMessage &&
                <Box
                sx={{
                    width: "95%",
                    my: 2,
                    borderRadius: 2,
                    borderColor: "#ccc",
                    backgroundColor: "#bababa",
                    padding: "2ch"
                }}>
                    <Box
                    sx={{
                        display: "flex",
                        flexDirection: "row"
                    }}>
                        <Typography
                            variant="body2"
                            fontWeight="bold"
                            sx={{
                                width: "95%"
                            }}
                        >
                            {!replyMessage.user ? "Аноним" : replyMessage.user.lastName + " " + replyMessage.user.firstName}
                        </Typography>
                        <Button
                            sx={{
                                padding: 0,
                                width: "5%"
                            }}
                            color='inherit'
                            onClick={() => setReplyMessage(undefined)}
                        >
                            <CloseOutlinedIcon/>
                        </Button>
                    </Box>
                    <Typography
                        noWrap
                        variant="body2"
                    >
                        {replyMessage.content}
                    </Typography>
                </Box>
                }
                <Box
                    sx={{
                    display: "flex",
                    flexDirection: "row"
                }}
                >
                    <InputField.Input 
                        label="Текст сообщения" 
                        value={message} 
                        onChange={setMessage}
                        autoFocus
                    />
                    <Button
                        onClick={sendMessage}
                        sx={{
                            padding: 0
                        }}
                    >
                        <SendOutlinedIcon/>
                    </Button>
                </Box>
            </Box>
            }
        </Box>
        }
        </>
    )
}

function showChatMessages(messages: ChatMessage[], chatInfo: ChatInfo, 
    setReplyMessage: Dispatch<SetStateAction<ChatMessage | undefined>>, getMessages: () => void){
    let list : ReactNode[] = [];
    if(chatInfo){ 
        messages.forEach(element => {
            const reply = messages.find(e => e.message_id === element.reply_message_id);
            list.push(<ChatMessageItem 
                message={element} 
                chatInfo={chatInfo} 
                setReplyMessage={setReplyMessage} 
                replyMessage={reply ? reply : element.reply_message_id !== null ? null : undefined}
                getMessages={getMessages}/>);
        });
    }
    return list;
}

export type ChatMessagesWindowProps = {
    chatInfo: ChatInfo | null,
}