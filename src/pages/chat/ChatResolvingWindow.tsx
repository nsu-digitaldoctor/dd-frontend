import { Box, FormControl, InputLabel, Typography, Select, Button, MenuItem } from '@mui/material';
import { ChatInfo, ChatMessage, EChatStatus } from '../../types/chatTypes';
import { ReactNode, useCallback, useEffect, useMemo, useState } from 'react';
import { useApi } from '../../hooks/use-api';
import { useStoreActions, useStoreState } from '../../hooks';
import { EUserRole } from '../../types';

export function ChatResolvingWindow(props: ChatResolvingWindowProps) {
    const api = useApi();
    const open = useStoreActions(actions => actions.popupModel.open);
    const userId = useStoreState(state => state.userModel.id);
    const [doctorId, setDoctorId] = useState(0);
    const menuList: ReactNode[] = useMemo(() => {
        return [];
    }, []);
    
    useEffect(() => {
        var doctorsList: {
            id: number,
            fn: string,
            ln: string
        }[] = [];
        props.messages.forEach((element)=> {
            if(element.user && element.user.role === EUserRole.DOCTOR && !doctorsList.find(e => e.id === element.user?.id)){
                doctorsList.push({
                    id: element.user.id,
                    fn: element.user.firstName,
                    ln: element.user.lastName
                });
                setDoctorId(element.user.id);
            }
        });
        menuList.length = 0;
        doctorsList.forEach((element) => {
            menuList.push(<MenuItem value={element.id}>{element.ln + " " + element.fn}</MenuItem>)
        });
    }, [props.messages, menuList]);
    
    return (
        <>
        <Box
        sx={{
            width: "50vw",
            mx: 5,
            my: 3
        }}>
            <Typography
                variant="h4"
                fontWeight="bold"
                paddingBottom={3}
            >
                Вам смогли помочь?
            </Typography>
            <Typography
                variant="h6"
                fontWeight="bold"
                paddingBottom={2}
            >
                Выберите врача, который вам помог:
            </Typography>
            <FormControl fullWidth>
                <InputLabel id="select-label">Врач</InputLabel>
                <Select
                value={doctorId}
                labelId="select-label"
                label="Врач"
                onChange={(e)=>{
                    setDoctorId(+e.target.value);
                }}
                >
                    <MenuItem value={0}>-</MenuItem>
                    {menuList}
                </Select>
            </FormControl>
        </Box>
        <Box
        sx={{
            display: "flex",
            justifyContent: "center",
            mx: 5,
            marginBottom: 4
        }}
        >
            <Button
                variant="contained"
                sx={{
                    textTransform: "none"
                }}
                onClick={() => {
                    if(props.chatInfo){
                        if(doctorId !== 0){
                            api.updateChat({
                                chats: [{
                                    chatId: props.chatInfo.id,
                                    newStatus: EChatStatus.RESOLVED,
                                    userId: doctorId,
                                }],
                                user: {
                                    id: userId
                                }
                            }).then(() => {
                                window.location.reload();
                            });
                        }else{
                            open({
                                render: () => (
                                    <Box
                                    sx={{
                                        mx: 5,
                                        my: 4
                                    }}>
                                        <Typography
                                            variant='h6'
                                        >
                                            Необходимо указать врача, который решил вашу проблему
                                        </Typography>
                                    </Box>
                                )
                            })
                        }
                    }
                }}
            >
                Закрыть обращение
            </Button>
        </Box>
        </> 
    );
}

export type ChatResolvingWindowProps = {
    chatInfo: ChatInfo,
    messages: ChatMessage[]
}