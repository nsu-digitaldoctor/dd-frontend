import { Box, Button, CssBaseline, Grid, Typography } from '@mui/material';
import { SideMenuWrapper } from '../SideMenuWrapper';
import { ReactNode, useCallback, useEffect, useState } from 'react';
import { ChatInfo, EChatStatus, chatsMapper } from '../../types/chatTypes';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import { ChatCreateWindow } from './ChatCreateWindow';
import { ChatMessagesWindow } from './ChatMessagesWindow';
import {Button as ButtonComp} from '../../components/Button';
import { useApi } from '../../hooks/use-api';
import { useStoreState } from '../../hooks';
import { EUserRole } from '../../types';

export function Chats() {
    const api = useApi();
    const {id: userId, role} = useStoreState(state => state.userModel);
    const [chatList, setChatList] = useState<ChatInfo[]>([]);
    const [chatWindow, setChatWindow] = useState<ReactNode | null>(null);
    const [currentChat, setCurrentChat] = useState<ChatInfo | null>();
    const [isLoaded, setIsLoaded] = useState(false);

    const createChatWindow = useCallback(() => {
        return(
            <ChatCreateWindow/>
        )
    }, []);

    const createChatInfoItem = useCallback((chatInfo: ChatInfo) => {
        return(
            <Button
                onClick={() => setCurrentChat(chatInfo)}
                sx={{
                    width: "100%",
                    textTransform: "none",
                    borderBottom: 1,
                    borderRadius: 0,
                    borderColor: "#ccc",
                    padding: 1,
                    paddingLeft: "5ch"
                }}
                color = "inherit"
            >
                <Box
                    sx={{
                        width: "100%"
                    }}
                >
                    {chatInfo.status === EChatStatus.NEW || chatInfo.status === EChatStatus.ANSWERED ?
                        <Typography
                            variant='body1'
                            fontWeight="bold"
                            textAlign="left"
                            width="100%"
                        >
                            {chatInfo.title}
                        </Typography>
                        :
                        chatInfo.status === EChatStatus.RESOLVED ? 
                        <Typography
                            variant='body1'
                            fontWeight="bold"
                            color= "gray"
                            textAlign="left"
                            width="100%"
                        >
                            {chatInfo.title + " (Решено)"}
                        </Typography>
                        :
                        <Typography
                            variant='body1'
                            fontWeight="bold"
                            color= "gray"
                            textAlign="left"
                            width="100%"
                        >
                            {chatInfo.title + " (Закрыто)"}
                        </Typography>
                    }   
                    <Typography
                        variant='body2'
                        color= "gray"
                        textAlign="left"
                        width="100%"
                        >
                        {chatInfo.createDate.toLocaleDateString()}
                    </Typography>
                </Box>
            </Button>
        )
    }, []);

    const showChatsList = useCallback((chats: ChatInfo[]) => {
        let list : ReactNode[] = [];
        chats.forEach(element => {
            list.push(createChatInfoItem(element));
        });
        return list;
    }, [createChatInfoItem]);

    useEffect(() => {
        if(userId !== -1){
            if(role === EUserRole.USER){
                api.getUserChatsList({
                    page: {
                        id: 0,
                        size: 1000
                    },
                    user: {
                        id: userId
                    },
                    filter: {}
                }).then((response) => {
                    setIsLoaded(true);
                    if(response !== undefined){
                        setChatList(chatsMapper(response));
                    }
                })
            }else{
                api.getDoctorChatsList({
                    page: {
                        id: 0,
                        size: 1000
                    },
                    user: {
                        id: userId
                    },
                    filter: {}
                }).then((response) => {
                    setIsLoaded(true);
                    if(response !== undefined){
                        setChatList(chatsMapper(response));
                    }
                })
            }
        }
    }, [api, setChatList, userId, role]);

    useEffect(() => {
        showChatsList(chatList);
    }, [chatList, showChatsList]);

    useEffect(() => {
        if(currentChat){
            setChatWindow(<ChatMessagesWindow chatInfo={currentChat}/>);
        }
    }, [currentChat]);

    return (
        <SideMenuWrapper>
            <Grid container sx={{height: "100vh"}}>
            <CssBaseline />
                <Grid
                    item
                    md={4}
                    sx={{
                        backgroundColor: "#fafafa",
                        maxHeight: "100vh",
                        overflow: "auto"
                    }}
                >
                    <Typography
                        variant='h5'
                        sx={{
                            mx: 3,
                            marginTop: 5,
                            height: "4vh"
                        }}
                        fontWeight="bold"
                    >
                        Мои обращения
                    </Typography>
                    <Box
                        sx={{
                            mx: 0,
                            my: 1,
                            height: "85vh"
                        }}
                    >
                        {isLoaded && 
                        <>
                            {chatList === undefined || chatList.length === 0? 
                            <Box
                                sx={{
                                    minHeight: "100%",
                                    display: "flex",
                                    paddingLeft: "2ch",
                                    flexDirection: "column",
                                    justifyContent: "center"
                                }}
                            >
                                <Typography
                                variant='h6'
                                color="gray"
                                >
                                    Здесь будет список чатов
                                </Typography>
                            </Box>
                            :
                            <Box
                                sx={{
                                    display: "flex",
                                    flexDirection: "column",
                                    textAlign: "left",
                                    overflow: "auto",
                                    maxHeight: "85vh"
                                }}
                            >
                                {showChatsList(chatList)}
                            </Box>
                            }
                            </>
                        }
                    </Box>
                    { role === EUserRole.USER &&
                    <ButtonComp 
                        renderComp={createChatWindow()}
                        startIcon={<AddOutlinedIcon/>}
                        sx={{
                            width: "30ch",
                            borderRadius: 5,
                            mx: 3
                        }}
                    >
                        Новое обращение
                    </ButtonComp>
                    }
                </Grid>
                <Grid
                    item
                    md={8}
                    sx={{
                        maxHeight: "100vh",
                        overflow: "auto"
                    }}
                >
                    {chatWindow}
                </Grid>
            </Grid>
        </SideMenuWrapper>
    );
};

