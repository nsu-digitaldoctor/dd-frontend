import { createUseStyles } from 'react-jss';

export default createUseStyles({
  appointmentsContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 48,
    marginTop: 30,
    gap: 10,
  }
});