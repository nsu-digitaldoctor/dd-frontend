import { Typography } from "@mui/material";
import { isAxiosError } from "axios";
import { FC, Fragment, useEffect } from "react";
import DoctorAppointmentCard from "../../components/organisms/DoctorAppointmentCard/DoctorAppointmentCard";
import { useStoreActions, useStoreState } from "../../hooks";
import { useApi } from "../../hooks/use-api";
import { SideMenuWrapper } from "../SideMenuWrapper";

import useStyles from './DoctorAppointments.styles';

const DoctorAppointments: FC = () => {
  const classes = useStyles();
  const api = useApi();

  const id = useStoreState(state => state.userModel.id);
  const currentAppointments = useStoreState(state => state.appointmentModel.currentAppointments);
  const setAppointments = useStoreActions(actions => actions.appointmentModel.setAppointments);
  const open = useStoreActions(actions => actions.popupModel.open);

  useEffect(() => {
    const fetchAppointmens = async () => {
      const response = await api.fetchDoctorAppointments(id);
      if (isAxiosError(response)) {
        open({
          render: () => (
            <Typography
            variant='body1'
            sx={{
              mx: 5,
              my: 3
            }}>
              Не удалось загрузить ваши приемы. Попробуйте позже!
            </Typography>
          )
        })
        return;
      }

      setAppointments(response);
    }

    if (id > 0) {
      fetchAppointmens();   
    }    
  }, [id]);

  return (
    <SideMenuWrapper>
      <Typography
        variant='h5'
        sx={{
          mx: 3,
          marginTop: 5,
          marginLeft: '48px',
          height: "4vh"
        }}
        fontWeight="bold"
      >
          Текущие приемы
      </Typography>
      {currentAppointments.length > 0
        ? 
        <div className={classes.appointmentsContainer}>
          {
            currentAppointments.map(appointment => 
              <Fragment key={appointment.id}>
                <DoctorAppointmentCard appointment={appointment} isCurrentAppointments={true} />
              </Fragment>
            )
          }
        </div>
        : <div className={classes.appointmentsContainer}><span>Список приемов пуст</span></div>
      }
    </SideMenuWrapper>
  );
}

export default DoctorAppointments;