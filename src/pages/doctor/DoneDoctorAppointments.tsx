import { Box, Button, Typography } from "@mui/material";
import { isAxiosError } from "axios";
import { FC, Fragment, useEffect, useMemo, useState } from "react";
import DoctorAppointmentCard from "../../components/organisms/DoctorAppointmentCard/DoctorAppointmentCard";
import { useStoreActions, useStoreState } from "../../hooks";
import { useApi } from "../../hooks/use-api";
import { EAppointmentsStatus, ESpecialization } from "../../types";
import { Appointment } from "../../types/appointmentsTypes";
import { SideMenuWrapper } from "../SideMenuWrapper";

import useStyles from './DoctorAppointments.styles';

const DoneDoctorAppointments: FC = () => {
  const classes = useStyles();
  const api = useApi();

  const id = useStoreState(state => state.userModel.id);
  const {archiveAppointments, needConclusionAppointments} = useStoreState(state => state.appointmentModel);
  const setAllAppointments = useStoreActions(actions => actions.appointmentModel.setAppointments);
  const open = useStoreActions(actions => actions.popupModel.open);
  const [isArchive, setArchive] = useState(false)

  const appointments = useMemo(() => {
    if (isArchive) {
      return archiveAppointments;
    } else {
      return needConclusionAppointments;
    }
  }, [archiveAppointments, needConclusionAppointments, isArchive])

  useEffect(() => {
    const fetchAppointmens = async () => {
      const response = await api.fetchDoctorAppointments(id);
      if (isAxiosError(response)) {
        open({
          render: () => (
            <Typography
            variant='body1'
            sx={{
              mx: 5,
              my: 3
            }}>
              Не удалось загрузить ваши приемы. Попробуйте позже!
            </Typography>
          )
        })
        return;
      }
      setAllAppointments(response);
    }

    if (id > 0) {
      fetchAppointmens();   
    }    
  }, [id]);

  return (
    <SideMenuWrapper>
      <Typography
        variant='h5'
        sx={{
          mx: 3,
          marginTop: 5,
          marginLeft: '48px',
          height: "4vh"
        }}
        fontWeight="bold"
      >
          Прошедшие приемы
      </Typography>
      <Box sx={{marginLeft: '40px', marginTop: '30px'}}>
        <Button
          sx={{
              borderRadius: 5
          }}
          onClick={() => {setArchive(false)}}
        >
          <Typography
            fontWeight="bold"
            fontSize={14}
            color={!isArchive ? "black" : "lightgray"}
          >
            Ожидают заключения
          </Typography>
        </Button>
        <Button
          sx={{
              borderRadius: 5
          }}
          onClick={() => {setArchive(true)}}
        >
          <Typography
            fontWeight="bold"
            fontSize={14}
            color={isArchive ? "black" : "lightgray"}
          >
            Архив
          </Typography>
        </Button>
      </Box>
      {appointments.length > 0
        ? 
        <div className={classes.appointmentsContainer}>
          {
            appointments.map(appointment => 
              <Fragment key={appointment.id}>
                <DoctorAppointmentCard appointment={appointment} isCurrentAppointments={false} />
              </Fragment>
            )
          }
        </div>
        : <div className={classes.appointmentsContainer}><span>Список приемов пуст</span></div>
      }
    </SideMenuWrapper>
  );
}

export default DoneDoctorAppointments;