import { Grid, Button, Box, Typography } from "@mui/material";
import { FC, useMemo, useState } from "react";
import { InputField } from "../../components/InputField";
import { checkEmail } from "../../helpers/credentialsCheckers";
import { useStoreState } from "../../hooks";
import { useApi } from "../../hooks/use-api";
import { SideMenuWrapper } from "../SideMenuWrapper";

const HospitalCabinet: FC = () => {
  const {IFT: origIFT, email: origEmail, name: origName} = useStoreState(state => state.hospitalModel)
  const [name, setName] = useState(origName);
  const [IFT, setIFT] = useState(origIFT);
  const [email, setEmail] = useState(origEmail);
  const api = useApi();

  const hasChanges = useMemo(() => {
    return IFT !== origIFT || email !== origEmail || name !== origName
  }, [IFT, email, name, origEmail, origIFT, origName])

  return (
    <SideMenuWrapper>
      <Typography
        variant='h5'
        sx={{
            mx: 3,
            marginTop: 5,
            marginLeft: '48px',
            height: "4vh"
        }}
        fontWeight="bold"
      >
        Личный кабинет
      </Typography>
      <Box component="form" noValidate sx={{ ml: '48px', width: 334 }}>
        <InputField.Input
          required
          value={name}
          label="Название компании"
          onChange={setName}
        />
        <InputField.Input
          required
          value={IFT}
          label="ИНН"
          onChange={setIFT}
        />
        <InputField.Input
          required
          value={email}
          label="Почта"
          onChange={setEmail}
          errorHandler={{
              errorText: "Неправильный формат почты",
              errorChecker: checkEmail
          }}
        />
        <Button
          onClick={() => console.log('update company data')}
          fullWidth
          disabled={!hasChanges}
          variant="contained"
          sx={{ mt: 3, mb: 2 }}
        >
          Сохранить
        </Button>
      </Box>
    </SideMenuWrapper>
  );
}

export default HospitalCabinet;