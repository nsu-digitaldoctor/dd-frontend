import { createUseStyles } from 'react-jss';

export default createUseStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: 24,
    gap: 24,
  },
  
  header: {
    display: 'flex',
    alignItems: 'flex-end',
    gap: 16,
  },

  doctorsInCompany: {
    fontSize: 16,
    lineHeight: '24px',
    letterSpacing: 0.5,
    color: '#49454F',
  },

  filtersContainer: {
    display: 'flex',
    gap: 16,
  },

  doctorList: {
    position: 'fixed',
    display: 'flex',
    flexDirection: 'column',
    top: '20vh',
    bottom: 0,
    overflowY: 'scroll',
    '& > span': {
      justifyContent: 'center',
    }
  }
});