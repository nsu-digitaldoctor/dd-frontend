import { FormControl, InputAdornment, InputLabel, MenuItem, Select, SelectChangeEvent, TextField, Typography } from "@mui/material";
import SearchIcon from '@mui/icons-material/Search';
import { FC, Fragment, useEffect, useState } from "react"
import HospitalDoctorCard from "../../components/organisms/HospitalDoctorCard/HospitalDoctorCard";
import { useStoreActions, useStoreState } from "../../hooks";
import { ESpecialization } from "../../types";
import { SideMenuWrapper } from "../SideMenuWrapper"

import useStyles from './HospitalsDoctors.styles';
import { useMemo } from "react";
import { useApi } from "../../hooks/use-api";

const HospitalsDoctors: FC = () => {
  const api = useApi();

  const classes = useStyles();
  const { doctors, id } = useStoreState(state => state.hospitalModel);
  const setDoctors = useStoreActions(actions => actions.hospitalModel.setHospitalDoctors);

  const [searchDoctor, setSearchDoctor]= useState('');
  const [spec, setSpec] = useState('');

  const handleChangeSpec = (event: SelectChangeEvent) => {
    setSpec(event.target.value);
  };

  const filteredDoctors = useMemo(() => {
    if (!spec && !searchDoctor) {
      return doctors;
    } else if (searchDoctor && !spec) {
      return doctors.filter(doctor => {
        const name = doctor.fullName;
        return name.toLowerCase().includes(searchDoctor.toLowerCase());
      })
    } else if (spec && !searchDoctor) {
      return doctors.filter(doctor => {
        const doctorSpec = doctor.specialization;
        return doctorSpec === spec;
      })
    } else {
      return doctors.filter(doctor => {
        const name = doctor.fullName;
        const doctorSpec = doctor.specialization;
        return name.toLowerCase().includes(searchDoctor.toLowerCase()) && doctorSpec === spec;
      })
    }
  }, [doctors, searchDoctor, spec])
  
  useEffect(() => {
    api.fetchHospitalDoctors(id).then(response => {
      setDoctors(response);
    })
  }, [api, id, setDoctors])

  return (
  <SideMenuWrapper>
    <div className={classes.root}>
      <div className={classes.header}>
        <Typography
            variant='h5'
            sx={{
                marginTop: 5,
                height: "4vh"
            }}
            fontWeight="bold"
        >
            Список врачей
        </Typography>
        {doctors.length > 0 
          ? <span className={classes.doctorsInCompany}>{`Врачей в компании: ${doctors.length}`}</span>
          : <span className={classes.doctorsInCompany}>В компании еще не зарегистрирован ни один врач</span>
        }
      </div>
      <div className={classes.filtersContainer}>
        <TextField
          placeholder="ФИО Врача"
          sx={{width: '636px'}}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
          value={searchDoctor}
          onChange={e => setSearchDoctor(e.target.value)}
          variant="filled"
        />
        <FormControl variant="filled" sx={{ minWidth: 240 }}>
          <InputLabel id="spec-select-label">Специализация врача</InputLabel>
          <Select
            labelId="spec-select-label"
            value={spec}
            onChange={handleChangeSpec}
          >
            <MenuItem value=""> <em>Специализация врача</em></MenuItem>
            {Object.entries(ESpecialization).map(([key, spec]) => 
            <MenuItem value={spec} key={key}>{spec}</MenuItem>)}
          </Select>
        </FormControl>
      </div>
      <div className={classes.doctorList}>
        {filteredDoctors.length > 0
          ? 
          <>
            {
              filteredDoctors.map(doctor => 
                <Fragment key={doctor.id}>
                  <HospitalDoctorCard doctor={doctor} />
                </Fragment>
              )
            }
          </>
          : <span>Список врачей пуст</span>
        }
      </div>
    </div>
  </SideMenuWrapper>);
}

export default HospitalsDoctors;