import { Box, Button, Grid, Typography } from '@mui/material';
import { Appointment } from '../../types/appointmentsTypes';
import { useApi } from '../../hooks/use-api';
import { EAppointmentsStatus} from '../../types';
import { useCallback } from 'react';
import { useStoreActions } from '../../hooks';
import { UpdateAppointmentWindow } from './UpdateAppointment';
import { getValueByKeyForSpecializationEnum } from '../../helpers/specializationsHelper';

export function AppointmentItem(props: AppointmentItemProps) {
    const api = useApi();
    const appointment = props.appointmentInfo;
    const status = appointment.status;
    const {open, close} = useStoreActions(actions => actions.popupModel);

    const deleteAppointment = useCallback(() => {
        open({
            render: () => (
                <Box
                sx={{
                    mx: 5,
                    my: 3
                }}>
                    <Typography
                    variant='h5'
                    fontWeight="bold">
                        Вы уверены, что хотите удалить запись?
                    </Typography>
                    <Box
                    sx={{
                        width: "100%",
                        justifyContent: "center",
                        display: "flex",
                        marginTop: 2
                    }}>
                        <Button
                        onClick={() => {
                            api.deleteAppointment({
                                id: appointment.id
                            }).then(() => {
                                window.location.reload();
                            })
                        }}>
                            Да
                        </Button>
                        <Button
                        variant='contained'
                        onClick={() => close()}>
                            Нет
                        </Button>
                    </Box>
                </Box>
            )
        })
    }, [api, appointment.id, close, open]);

    return (
        <Grid container 
            sx={{width: "100%",
                heigth: "50%",
                my: 2,
                border: 1,
                borderRadius: 2,
                padding: 2}}>
            <Grid 
                item 
                md={9}>
                <Box>
                    <Typography
                        variant='h6'
                        fontWeight="bold"
                        sx={{
                            paddingRight: 2,
                        }}
                    >
                        {appointment.application}
                    </Typography>
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                        }}
                    >
                        <Typography
                        sx={{
                            height: "100%",
                            paddingTop: 0.5
                        }}>
                            {"Врач: " + appointment.doctor.lastName + " " + appointment.doctor.firstName + " " 
                                + (appointment.doctor.middleName ? appointment.doctor.middleName : "")}
                        </Typography>
                        <Typography
                            variant='body2'
                            sx={{
                                height: "100%",
                                backgroundColor: "#eee",
                                borderRadius: 2,
                                marginLeft: 2,
                                padding: 0.5
                            }}
                        >
                            {appointment.doctor.specialty !== null ? getValueByKeyForSpecializationEnum(appointment.doctor.specialty) : "Без специализации"}
                        </Typography>
                    </Box>
                    <Typography
                        variant='body1'
                    >
                        {appointment.doctor.hospitalName + 
                        (appointment.doctor.hospitalAddress && appointment.doctor.hospitalAddress !== null ? " : " + appointment.doctor.hospitalAddress : "")}
                    </Typography>
                    <Typography
                        variant='body1'
                    >
                        {appointment.date.toLocaleString()}
                    </Typography>
                    {!(status === EAppointmentsStatus.OPEN || status === EAppointmentsStatus.ACCEPTED || status === EAppointmentsStatus.REJECTED) &&
                    <Box>
                        <Typography
                            variant='h6'
                            color="#1976D2">
                            Заключение
                        </Typography>
                        <Typography
                            variant='body2'
                            overflow="auto">
                            {appointment.conclusion !== "" ? appointment.conclusion : "Заключения пока нет"}
                        </Typography>
                    </Box>
                    }
                </Box>
            </Grid>
            <Grid
                item
                md={3}>
                {(status === EAppointmentsStatus.OPEN || status === EAppointmentsStatus.ACCEPTED) &&
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <Button
                    sx={{
                        textTransform: "none",
                    }}
                    onClick={deleteAppointment}>
                        <Typography
                        sx={{
                            '&:hover':{
                                color: "red"
                            },
                            color: "lightgray"
                        }}>
                            Отменить запись
                        </Typography>
                    </Button>
                    <Button
                        variant='contained'
                        sx={{
                            textTransform: "none"
                        }}
                        onClick={() => open({
                            render: () => (<UpdateAppointmentWindow appointment={appointment}/>)
                        })}
                    >
                        Изменить информацию о приеме
                    </Button>
                </Box>
                }
            </Grid>
        </Grid>
    );
};

export type AppointmentItemProps = {
    appointmentInfo: Appointment
}