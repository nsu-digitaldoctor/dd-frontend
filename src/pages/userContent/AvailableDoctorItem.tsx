import { Box, Button, Grid, Typography } from '@mui/material';
import { DoctorInfo } from '../../types/appointmentsTypes';
import { CreateAppointmentWindow } from './CreateAppointmentPage';
import { useStoreActions } from '../../hooks';
import { useRoute } from '../../hooks/useRoute';
import { useCallback } from 'react';
import { getValueByKeyForSpecializationEnum } from '../../helpers/specializationsHelper';

export function AvailableDoctorItem(props: AvailableDoctorItemProps) {
    const doctorInfo = props.doctorInfo;
    const open = useStoreActions(actions => actions.popupModel.open);
    const route = useRoute();

    const routeToAppointments = useCallback(() => {
        route.toUserAppointments();
    }, [route])

    return (
        <Grid container 
            sx={{width: "100%",
                heigth: "50%",
                my: 2,
                border: 1,
                borderRadius: 2,
                padding: 2}}>
            <Grid 
                item 
                md={9}>
                <Box>
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            paddingBottom: 2
                        }}
                    >
                        <Typography
                            variant='h6'
                            fontWeight="bold"
                            sx={{
                                paddingRight: 2,
                            }}
                        >
                            {doctorInfo.lastName + " " + doctorInfo.firstName + " " + (doctorInfo.middleName ? doctorInfo.middleName : "")}
                        </Typography>
                        <Typography
                            variant='body2'
                            sx={{
                                height: "100%",
                                backgroundColor: "#eee",
                                borderRadius: 2,
                                padding: 1,
                            }}
                        >
                            {doctorInfo.specialty !== null ? getValueByKeyForSpecializationEnum(doctorInfo.specialty) : "Без специализации"}
                        </Typography>
                    </Box>
                    <Typography
                        variant='body1'
                    >
                        {doctorInfo.hospitalName + 
                        (doctorInfo.hospitalAddress && doctorInfo.hospitalAddress !== null ? " : " + doctorInfo.hospitalAddress : "")}
                    </Typography>
                </Box>
            </Grid>
            <Grid
                item
                md={3}>
                <Box
                    sx={{
                        display: "flex",
                        flexDirection: "column",
                        alignContent: "right",
                        marginLeft: 2
                    }}
                >
                    <Typography
                        variant='h5'
                        fontWeight="bold"
                        sx={{
                            textAlign: "right",
                            paddingBottom: 2
                        }}
                    >
                        {doctorInfo.price + " р."}
                    </Typography>
                    <Button
                        variant='contained'
                        sx={{
                            textTransform: "none"
                        }}
                        onClick={() => open({
                            render: () => (<CreateAppointmentWindow doctor={doctorInfo} onClose={routeToAppointments}/>)
                        })}
                    >
                        Выбрать дату приема
                    </Button>
                </Box>
            </Grid>
        </Grid>
    );
};

export type AvailableDoctorItemProps = {
    doctorInfo: DoctorInfo
}