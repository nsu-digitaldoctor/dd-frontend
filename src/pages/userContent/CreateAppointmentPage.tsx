import { Box, Button, TextField, Typography } from '@mui/material';
import { useCallback, useState } from 'react';
import { DoctorInfo } from '../../types/appointmentsTypes';
import { InputField } from '../../components/InputField';
import { useApi } from '../../hooks/use-api';
import { useStoreActions, useStoreState } from '../../hooks';
import { getValueByKeyForSpecializationEnum } from '../../helpers/specializationsHelper';

export function CreateAppointmentWindow(props: CreateAppointmentWindowProps) {
    const api = useApi(); 
    const {id, userName} = useStoreState(state => state.userModel);
    const [application, setApplication] = useState("");
    const currentDate = new Date();
    const close = useStoreActions(actions => actions.popupModel.close);
    currentDate.setDate(currentDate.getDate()+1);
    currentDate.setHours(currentDate.getHours() - currentDate.getTimezoneOffset()/60)
    const [date, setDate] = useState(currentDate.toISOString().substring(0, currentDate.toISOString().lastIndexOf(":")));

    const createAppointment = useCallback(() => {
        api.createAppointment({
            application: application,
            date: date,
            doctorId: props.doctor.id,
            userId: id
        }).then(() => {
            close();
            props.onClose();
        })
    }, [api, application, date, props.doctor, id, userName, close, props.onClose]);

    return (
        <Box
        sx={{
            mx: 5,
            my: 3,
        }}>
            <Typography
                variant='h5'
                fontWeight="bold"
            >
                Напишите тему приема и выберите дату и время
            </Typography>
            <Typography
            fontWeight="bold">
                Врач: 
            </Typography>
            <Typography>
                {props.doctor.lastName + " " + props.doctor.firstName + (props.doctor.middleName ? " " +props.doctor.middleName : "") 
                 + (props.doctor.specialty !== null ? " | " + getValueByKeyForSpecializationEnum(props.doctor.specialty) : "")}
            </Typography>
            <Typography
            fontWeight="bold">
                Клиника: 
            </Typography>
            <Typography>
                {props.doctor.hospitalName + 
                        (props.doctor.hospitalAddress && props.doctor.hospitalAddress !== null ? " : " + props.doctor.hospitalAddress : "")}
            </Typography>
            <div>
            <InputField.Input
                value={application}
                onChange={setApplication}
                label='Тема приема'
                errorHandler={{
                    errorChecker: (val: string) => {return val.length >= 3},
                    errorText: "Тема приема слишком короткая"
                }}
                sx={{
                    width: "70ch",
                    marginRight: 2
                }}
            />
            <TextField
                type="datetime-local"
                value={date}
                onChange={e => setDate(e.target.value)}
                label="Дата и время"
                margin='normal'
            />
            </div>
            <Box
            sx={{
                width: "100%",
                justifyContent: "center",
                display: "flex",
            }}>
                <Button
                    variant='contained'
                    onClick={createAppointment}
                    disabled={application.length < 3}
                >
                    Создать запись
                </Button>
            </Box>
        </Box>
    );
};

export type CreateAppointmentWindowProps = {
    doctor: DoctorInfo,
    onClose: () => void
}