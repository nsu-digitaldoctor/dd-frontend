import { Box, Button, TextField, Typography } from '@mui/material';
import { useCallback, useState } from 'react';
import { Appointment} from '../../types/appointmentsTypes';
import { InputField } from '../../components/InputField';
import { useApi } from '../../hooks/use-api';
import { useStoreState } from '../../hooks';
import { getValueByKeyForSpecializationEnum } from '../../helpers/specializationsHelper';

export function UpdateAppointmentWindow(props: UpdateAppointmentWindowProps) {
    const api = useApi(); 
    const appointment = props.appointment;
    const {id} = useStoreState(state => state.userModel);
    const [application, setApplication] = useState(appointment.application);
    const currentDate = new Date(appointment.date);
    currentDate.setHours(currentDate.getHours() - currentDate.getTimezoneOffset()/60);
    const [date, setDate] = useState(currentDate.toISOString().substring(0, currentDate.toISOString().lastIndexOf(":")));

    const updateAppointment = useCallback(() => {
        api.updateAppointment({
            id: appointment.id,
            application: application,
            date: date,
            doctorId: appointment.doctor.id,
            userId: id
        }).then(() => {
            window.location.reload();
        })
    }, [api, application, date, id, appointment.doctor.id, appointment.id]);

    return (
        <Box
        sx={{
            mx: 5,
            my: 3,
        }}>
            <Typography
                variant='h5'
                fontWeight="bold"
            >
                Информация о приеме
            </Typography>
            <Typography
            fontWeight="bold">
                Врач: 
            </Typography>
            <Typography>
                {appointment.doctor.lastName + " " + appointment.doctor.firstName + (appointment.doctor.middleName ? " " +appointment.doctor.middleName : "") 
                 + (appointment.doctor.specialty !== null ? " | " + getValueByKeyForSpecializationEnum(appointment.doctor.specialty) : "")}
            </Typography>
            <Typography
            fontWeight="bold">
                Клиника: 
            </Typography>
            <Typography>
                {appointment.doctor.hospitalName + 
                (appointment.doctor.hospitalAddress && appointment.doctor.hospitalAddress !== null ? " : " + appointment.doctor.hospitalAddress : "")}
            </Typography>
            <div>
            <InputField.Input
                value={application}
                onChange={setApplication}
                label='Тема приема'
                errorHandler={{
                    errorChecker: (val: string) => {return val.length >= 3},
                    errorText: "Тема приема слишком короткая"
                }}
                sx={{
                    width: "70ch",
                    marginRight: 2
                }}
            />
            <TextField
                type="datetime-local"
                value={date}
                onChange={e => setDate(e.target.value)}
                label="Дата и время"
                margin='normal'
            />
            </div>
            <Box
            sx={{
                width: "100%",
                justifyContent: "center",
                display: "flex",
            }}>
                <Button
                    variant='contained'
                    onClick={updateAppointment}
                    disabled={application.length < 3 
                        || (currentDate.toISOString().substring(0, currentDate.toISOString().lastIndexOf(":")) === date
                    && application === appointment.application)}
                >
                    Сохранить изменения
                </Button>
            </Box>
        </Box>
    );
};

export type UpdateAppointmentWindowProps = {
    appointment: Appointment
}