import { Box, Button, Typography } from '@mui/material';
import { SideMenuWrapper } from '../SideMenuWrapper';
import { ReactNode, useEffect, useMemo, useState } from 'react';
import { Appointment, appointmentsMapper } from '../../types/appointmentsTypes';
import { useApi } from '../../hooks/use-api';
import { EAppointmentsStatus } from '../../types';
import { AppointmentItem } from './AppointmentItem';
import { useStoreState } from '../../hooks';

export function UserAppointmentsPage() {
    const api = useApi();
    const {id} = useStoreState(state => state.userModel);
    const [current, setCurrent] = useState<AppointmentType>(AppointmentType.FUTURE);
    const [appointments, setAppointents] = useState<Appointment[]>([]);

    useEffect(() => {
        if(id !== -1){
            api.getUserAppointments(id).then((response) => {
                if(response){
                    setAppointents(appointmentsMapper(response));
                }
            });
        }
    }, [api, setAppointents, id]);

    const filterAppointments = useMemo(() => {
        if(current === AppointmentType.FUTURE){
            return appointments.filter((appointment) => {
                return appointment.status === EAppointmentsStatus.OPEN || appointment.status === EAppointmentsStatus.ACCEPTED
            }).sort((a, b) => {
                if(a.status === b.status){
                    return new Date(a.date) < new Date(b.date) ? 1 : -1;
                }else{
                    if(a.status === EAppointmentsStatus.ACCEPTED){
                        return -1;
                    }else{
                        return 1;
                    }
                }
            });
        }else{
            return appointments.filter((appointment) => {
                return appointment.status === EAppointmentsStatus.DONE || appointment.status === EAppointmentsStatus.REJECTED || appointment.status === EAppointmentsStatus.EXPIRED
            }).sort((a, b) => {
                if(a.status === b.status){
                    return new Date(a.date) < new Date(b.date) ? 1 : -1;
                }else{
                    if(a.status === EAppointmentsStatus.DONE 
                        || (a.status === EAppointmentsStatus.EXPIRED && b.status === EAppointmentsStatus.REJECTED)){
                        return -1;
                    }else{
                        return 1;
                    }
                }
            });;
        }
    }, [appointments, current]);

    return (
        <SideMenuWrapper>
            <Box
            sx={{
                mx: 5,
                my: 5
            }}>
                <Box>
                    <Button
                        sx={{
                            borderRadius: 5
                        }}
                        onClick={() => {setCurrent(AppointmentType.FUTURE)}}
                    >
                        <Typography
                            variant='h5'
                            fontWeight="bold"
                            color={current === AppointmentType.FUTURE ? "black" : "lightgray"}
                        >
                            Предстоящие приемы
                        </Typography>
                    </Button>
                    <Button
                        sx={{
                            borderRadius: 5
                        }}
                        onClick={() => {setCurrent(AppointmentType.PAST)}}
                    >
                        <Typography
                            variant='h5'
                            fontWeight="bold"
                            color={current === AppointmentType.PAST ? "black" : "lightgray"}
                        >
                            Прошедшие приемы
                        </Typography>
                    </Button>
                </Box>
                {appointments && filterAppointments.length > 0 ? appointmentsPage(filterAppointments) : 
                <Box
                sx={{
                    display: "flex",
                    flexDirection: "column",
                    minHeight: "85vh",
                    justifyContent: "center"
                }}>
                    <Typography
                        variant='h6'
                        color="gray"
                        textAlign="center"
                    >
                        {current === AppointmentType.FUTURE ? "Здесь будут ваши будущие приемы" : "Здесь будут ваши прошедшие приемы"}
                    </Typography>
                </Box>
                }
            </Box>
        </SideMenuWrapper>
    );
};

function appointmentsPage(appointments: Appointment[]){
    const listNodes : ReactNode[] = [];
    appointments.forEach((element) => {
        listNodes.push(<AppointmentItem appointmentInfo={element}/>)
    });

    return(
        <Box
        sx={{
            height: "85vh",
            overflow: "auto"
        }}>
            {listNodes}
        </Box>
    )
}

enum AppointmentType{
    FUTURE,
    PAST
}