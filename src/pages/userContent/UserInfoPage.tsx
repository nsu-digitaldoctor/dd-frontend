import React, { useEffect, useState } from 'react';
import { SideMenuWrapper } from '../SideMenuWrapper';
import { Box, Button, TextField, Typography } from '@mui/material';
import { useApi } from '../../hooks/use-api';

export function UserInfoPage() {
    const api = useApi();
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [middleName, setMiddleName] = useState("");
    const [email, setEmail] = useState("");

    useEffect(() => {
        api.account().then((response) => {
            setFirstName(response.firstName);
            setLastName(response.lastName);
            setMiddleName(response.middleName ? response.middleName : "");
            setEmail(response.email);
        })
    }, [api]);

    return (
        <SideMenuWrapper>
            <Box
            sx={{
                mx: 5,
                my: 5
            }}>
                <Typography
                    variant='h6'
                    fontWeight="bold"
                >
                    Личный кабинет
                </Typography>
                <Box
                sx={{
                    my: 2,
                    display: "flex",
                    flexDirection: "column",
                    width: "40ch"
                }}>
                    <TextField
                        label='Имя'
                        value={firstName}
                        onChange={e => setFirstName(e.target.value)}
                        required
                        sx={{
                            my: 1
                        }}
                        variant='filled'
                    />
                    <TextField
                        label='Фамилия'
                        value={lastName}
                        onChange={e => setLastName(e.target.value)}
                        required
                        sx={{
                            my: 1
                        }}
                        variant='filled'
                    />
                    <TextField
                        label='Отчество'
                        value={middleName}
                        onChange={e => setMiddleName(e.target.value)}
                        sx={{
                            my: 1
                        }}
                        variant='filled'
                    />
                    <TextField
                        label='Почта'
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                        type="email"
                        sx={{
                            my: 1
                        }}
                        variant='filled'
                    />
                    <Button
                    sx={{
                        borderRadius: 2,
                        textTransform: "none"
                    }}
                    variant='contained'>
                        Сохранить
                    </Button>
                </Box>
            </Box>
        </SideMenuWrapper>
    );
};