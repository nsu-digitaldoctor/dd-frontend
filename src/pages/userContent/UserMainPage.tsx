import { ReactNode, useEffect, useMemo, useState } from 'react';
import { SideMenuWrapper } from '../SideMenuWrapper';
import { Box, FormControl, InputLabel, MenuItem, Select, TextField, Typography } from '@mui/material';
import { DoctorInfo } from '../../types/appointmentsTypes';
import { useApi } from '../../hooks/use-api';
import { AvailableDoctorItem } from './AvailableDoctorItem';
import { ESpecialization } from '../../types';

export function UserMainPage() {
    const api = useApi();
    const [searchString, setSearchString] = useState("");
    const [occupation, setOccupation] = useState("");
    const [availableDoctors, setAvailableDoctors] = useState<DoctorInfo[]>([]);
    const [isLoaded, setIsLoaded] = useState(false);

    useEffect(() => {
        api.getDoctors().then((response) => {
            setAvailableDoctors(response as DoctorInfo[]);
            setIsLoaded(true);
        })
    }, [api, setAvailableDoctors, setIsLoaded]);

    const filteredDoctors = useMemo(() => {
        if (!occupation && !searchString) {
          return availableDoctors;
        } else if (searchString && !occupation) {
          return availableDoctors.filter(doctor => {
            const name = doctor.firstName + " " + doctor.lastName + " " + doctor.middleName;
            const hospitalName = doctor.hospitalName;
            const hospitalAddress = doctor.hospitalAddress;
            return name.toLowerCase().includes(searchString.toLowerCase()) 
            || hospitalName?.toLowerCase().includes(searchString.toLowerCase())
            || hospitalAddress?.toLowerCase().includes(searchString.toLowerCase());
          })
        } else if (occupation && !searchString) {
          return availableDoctors.filter(doctor => {
            const doctorSpec = doctor.specialty;
            return doctorSpec === occupation;
          })
        } else {
          return availableDoctors.filter(doctor => {
            const name = doctor.firstName + " " + doctor.lastName + " " + doctor.middleName;
            const doctorSpec = doctor.specialty;
            const hospitalName = doctor.hospitalName;
            const hospitalAddress = doctor.hospitalAddress;
            return (name.toLowerCase().includes(searchString.toLowerCase())
            || hospitalName?.toLowerCase().includes(searchString.toLowerCase())
            || hospitalAddress?.toLowerCase().includes(searchString.toLowerCase())
            ) && doctorSpec === occupation;
          })
        }
      }, [availableDoctors, searchString, occupation])

    return (
        <SideMenuWrapper>
            <Box
                sx={{
                    mx: 5,
                    my: 5
                }}>
                <Typography
                    variant='h5'
                    fontWeight="bold"
                    sx={{
                        paddingBottom: 2
                    }}
                >
                    Выберите врача и дату приема
                </Typography>
                <TextField 
                    variant='filled'
                    type='search'
                    label="ФИО Врача, Название или Адрес клиники"
                    value={searchString}
                    onChange={(e) => {
                        setSearchString(e.target.value);
                    }}
                    autoFocus
                    sx={{
                        width: "75%"
                    }}
                />
                <FormControl sx={{width: "20%", marginLeft: 2, marginBottom: 2}}>
                    <InputLabel id="select-label">Специальность</InputLabel>
                    <Select
                        value={occupation}
                        labelId="select-label"
                        label="Специальность"
                        onChange={(e)=>{
                            setOccupation(e.target.value);
                        }}
                    >
                        <MenuItem value=""> <em>Специализация врача</em></MenuItem>
                        {Object.entries(ESpecialization).map(([key, spec]) => 
                        <MenuItem value={key} key={key}>{spec}</MenuItem>)}
                    </Select>
                </FormControl>
                {isLoaded &&
                <>
                <Typography
                >
                    {"Всего врачей: " + filteredDoctors.length}
                </Typography>
                <Box
                    sx={{
                        maxHeight: "73vh",
                        display: "flex",
                        flexDirection: "column",
                        overflow: "auto",
                        marginTop: 2
                    }}
                >
                    {showAvailableDoctors(filteredDoctors)}
                </Box>
                </>
                }
            </Box>
        </SideMenuWrapper>
    );
};

function showAvailableDoctors(availableDoctors: DoctorInfo[]){
    var list : ReactNode[] = [];
    availableDoctors.forEach((element) => {
        list.push(<AvailableDoctorItem doctorInfo={element}/>)
    });
    return list;
}