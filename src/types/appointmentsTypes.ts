import { IGetUserAppointmentsResponseRaw } from "../core/api/mappers"
import { EAppointmentsStatus } from "./common"
import { ESpecialization } from "./hospital";

export function appointmentsMapper(appointments: IGetUserAppointmentsResponseRaw['data']){
    var list: Appointment[] = [];
    appointments.forEach(element => {
        list.push({
            id: element.id,
            user: element.user,
            doctor: element.doctor,
            status: element.status,
            application: element.application,
            conclusion: element.conclusion,
            date: new Date(element.date)
        })
    });
    return list;
}

export type DoctorInfo = {
    id: number,
    firstName: string,
    lastName: string,
    middleName?: string,
    specialty: ESpecialization,
    price: number,
    hospitalName: string,
    hospitalAddress: string
}

export type Appointment = {
    id: number,
    user: {
        id: number,
        firstName: string,
        lastName: string,
        middleName?: string
    },
    doctor: DoctorInfo,
    application: string,
    status: EAppointmentsStatus,
    conclusion: string | null,
    date: Date
}