import { IChatInfoResponse, IGetMessagesListResponse} from "../core/api/mappers"
import { EUserRole } from "./common";

export function chatsMapper(chats: IChatInfoResponse){
    var list : ChatInfo[] = [];
    if(chats.chats){
        chats.chats.forEach((element) => {
            list.push({
                id: element.id,
                createDate: new Date(element.createDate),
                isAnonymous: element.isAnonymous,
                status: element.status,
                user_id: element.user ? element.user.id : 0,
                title: element.title
            });
        });
    }
    list.sort((e1, e2) => {
        if(((e1.status === EChatStatus.NEW || e1.status === EChatStatus.ANSWERED) && (e2.status === EChatStatus.NEW || e2.status === EChatStatus.ANSWERED))
        || ((e1.status === EChatStatus.CLOSED || e1.status === EChatStatus.RESOLVED) && (e2.status === EChatStatus.CLOSED || e2.status === EChatStatus.RESOLVED))){
            return e2.createDate.getDate() + e2.createDate.getTime() - e1.createDate.getUTCDate() - e1.createDate.getTime();
        }else{
            if((e1.status === EChatStatus.NEW || e1.status === EChatStatus.ANSWERED) && (e2.status === EChatStatus.CLOSED || e2.status === EChatStatus.RESOLVED)){
                return -1;
            }else{
                return 1;
            }
        }
        
    });
    return list;
}

export function messagesMapper(messages: IGetMessagesListResponse){
    var list: ChatMessage[] = [];
    if(messages.messages){
        messages.messages.forEach((element) => {
            list.push({
                message_id: element.messageId,
                chat_id: element.chatId,
                user: element.user !== null ? {
                    id: element.user.id,
                    firstName: element.user.firstName,
                    lastName: element.user.lastName,
                    role: element.user.role as EUserRole
                } : null,
                content: element.content,
                reply_message_id: element.replyMessageId,
                sent_date: new Date(element.sentDate)
            });
        })
    }
    return list;
}

export enum EChatStatus{
    NEW = "NEW",
    RESOLVED = "RESOLVED",
    CLOSED = "CLOSED",
    ANSWERED = "ANSWERED"
}

export type ChatInfo = {
    id: number,
    title: string,
    user_id: number,
    isAnonymous: boolean,
    status: EChatStatus,
    createDate: Date,
    doctor_id?: number
}

export type ChatMessage = {
    message_id: number,
    chat_id: number,
    user: {
        id: number,
        firstName: string,
        lastName: string,
        role: EUserRole
    } | null,
    content: string,
    sent_date: Date,
    reply_message_id: number | null
}