export const FEATURE_FLAG_NAMES = [];

export type TFeatureFlagName = (typeof FEATURE_FLAG_NAMES)[number];

export enum EUserRole {
  ORG = 'ORG',
  USER = 'USER',
  DOCTOR = 'DOCTOR',
}

export enum EAppointmentsStatus {
  OPEN = 'OPEN',
  ACCEPTED = 'ACCEPTED',
  REJECTED = 'REJECTED',
  DONE = 'DONE',
  EXPIRED = 'EXPIRED',
}

export type TFeatureFlags = Record<TFeatureFlagName, boolean>;

export type TLocale = 'ru' | 'en';

export type TTheme = 'dark' | 'light';

export type ValueOf<
  T extends readonly any[] | ArrayLike<any> | Record<any, any>,
> = T extends readonly any[]
  ? T[number]
  : T extends ArrayLike<any>
  ? T[number]
  : T extends object
  ? T[keyof T]
  : never;