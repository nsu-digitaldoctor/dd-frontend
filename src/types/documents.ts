export interface IPasport {
  serialNumber: string;
  number: string;
}

// Здесь добавлять новые типы документов
export type IDocument = IPasport;