import { ReactNode } from 'react';
import { ValueOf } from './common';

export type TPopupSize = 'xl' | 'l' | 'm' | 's';
export type TPopupPosition = 'top' | 'center' | 'bottom';

export const PopupTypes = {
  FULL_SCREEN: 'fullScreen',
  FULL_HEIGHT: 'fullHeight',
} as const;
export type TPopupType = ValueOf<typeof PopupTypes>;

export interface IPopup {
  type?: TPopupType;
  size?: TPopupSize;
  /**
   * @default `top`
   */
  position?: TPopupPosition;
  /**
   * Рисовать ли background для модалки
   * @default true
   */
  withBackground?: boolean;
  // если shouldHidePreviousPopups = true, предыдущие модалки скрываются
  // но остаются в очереди и выводятся, если закрыть текущую модалку
  shouldHidePreviousPopups?: boolean;
  render(): ReactNode;
  onClose?(): void;
}

export default IPopup;