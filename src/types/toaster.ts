import { TOptions } from 'i18next';
import { ReactNode } from 'react';

export declare type ToasterType = 'error' | 'info' | 'warning' | 'ok' | 'not-ok';

export interface IToaster {
  /**
   * @default error
   */
  type?: ToasterType;
  /**
   * Заголовок
   */
  title?: string;
  /**
   * Сообщение
   */
  text?: string;
  /**
   * Таймаут
   * @default 7000
   */
  timeout?: number;
  /**
   * Для ReactNode
   */
  children?: ReactNode;
  /**
   * Для сообщений содержащих верстку
   */
  innerHtmlText?: string;
  /**
   * Название запроса. Используется для глобальных тостеров из запросов api
   */
  requestName?: string;
  /**
   * Код ошибки. Используется для глобальных тостеров из запросов api
   */
  errorCode?: string;
  /**
   * Для сообщений из локали toaster-messages
   */
  messageCode?: string;
  /**
   * Переменные для локализованного сообщения
   */
  messageVariables?: TOptions;
  /**
   * Используется в качестве уникального поля, по которому можно удалить тостер
   * вручную (см. removeToaster в global-tech-model)
   */
  timestamp: number;
}
