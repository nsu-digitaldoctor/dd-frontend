export type TValidationError = string[];

export type ErrorsType<V> = {
  [K in keyof V]?: TValidationError;
};